import React from 'react';
import logo from './logo.svg';
import './App.css';
import Navigation from './components/customWidgets/navbar';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';

import InvoiceGrid from './components/jqwidgets/InvoiceGrid.tsx';
import HeaderPage from './components/customWidgets/headerPage';
import Add from './assets/icons/Add.svg';
import SearchBar from './components/customWidgets/searchBar';
import './styles/firstPageStyle.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import BtnAdd from './components/btnAdd';
import MainPage from './components/customWidgets/mainPage';
import NewInvoicePage from './components/customWidgets/newInvoicePage';
import CheckInvoicePage from './components/customWidgets/checkInvoicePage';
import { HashRouter as Router, Route, Link, Switch } from "react-router-dom";

function mainPage(){
  return <MainPage/>
}

function newInvoicePage(){
  return <NewInvoicePage/>
}
function App() {

  return (
    // <MainPage/>
    // <NewInvoicePage/>
    <Router baseline="/">
      <Switch>
        <Route exact path="/" component={MainPage} />
        <Route path="/newInvoice" component={NewInvoicePage} />
        <Route path="/checkInvoice" component={CheckInvoicePage} />
      </Switch>
    </Router>
  );
}

export default App;
