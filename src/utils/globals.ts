export let productList = [
    {
        "Code": "51234",
        "Description": "Combo Lata de pintura x Galón - Blanco Esmaltado Combo Lata de pintura x Galón - Blanco Esmaltado Combo Lata de pintura x Galón - Blanco Esmaltado Combo Lata de pintura x Galón - Blanco Esmaltado",
        "UnitValue": 12.40
    },
    {
        "Code": "41234",
        "Description": "Combo Lata de pintura x Galón - Verde Esmaltado",
        "UnitValue": 42.40
    },
    {
        "Code": "31234",
        "Description": "Combo Lata de pintura x Galón - Gris Esmaltado",
        "UnitValue": 11.40
    },
    {
        "Code": "21234",
        "Description": "Combo Lata de pintura x Galón - Azul Esmaltado",
        "UnitValue": 32.40
    },
    {
        "Code": "12345",
        "Description": "Combo Lata de pintura x Galón - Amarillo Esmaltado",
        "UnitValue": 132.40
    },
]