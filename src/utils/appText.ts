export let headerTable = {
    CreationDate: "Fecha de Creación",
    Series: "Consecutivo",
    Customer: "Cliente",
    TotalAmount: "Valor Total",
    Status: "Estado Recepción",
    Payment: "Pago",
    Send: "Emitir",
    Pdf: "PDF",
    Xml: "XML",
    Detail: "Detalle"
}

export let tooltipText = {
    attachmentText: "Archivo adjunto",
    creditDevitNotes: "Notas de crédito o débito asociada",
    receiptStatus: {
        step1: "Acuse de recibo",
        step2: "Entrega de producto",
        step3: "Título Valor",
        failStep: "Rechazada"
    },
    paymentStatus: {
        totalPayment: "Pago Total",
        partialPayment: "Pago parcial / Abono",
        duePayment: "Vencida"
    },
    download: {
        invoice: "Descargar factura",
        xml: "Descargar XML"
    }
}

export let headerInvoiceTable = {
    Code: "Código",
    Description: "Descripción",
    Qty: "Cantidad",
    UnitValue: "Valor Unitario",
    Discount: "Descuento",
    Total: "Valor Total",
}

export let modalNewInvoice = {
    sendInvoice: "Emitir factura",
    downloadInovice: "Descargar PDF",
    deleteInvoice: "Eliminar factura"
}

export let modalCheckInvoice = {
    sendInvoice: "Reenviar factura",
    downloadXml: "Descargar XML",
    downloadInovice: "Descargar PDF",
    deleteInvoice: "Eliminar factura"
}