import React, { Component } from 'react';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';

// import Back from './assets/icons/Back.svg';
import Alert from '../../assets/icons/Alert.svg';
import ArrowLeft from '../../assets/icons/ArrowLeft.svg';

import '../../styles/newProduct.css';
import * as utils from '../../utils/utils'
import Help from '../../assets/icons/Help.svg';
import SearchInvoice from './quentaInput';
import Calendar from '../../assets/icons/Calendar.svg';
class NewProduct extends Component {
    render() {
        return (
            <div className="newProduct alerta_popup">
                <Row>
                <p className="titleNewClient">
                    <span>  <img src={ArrowLeft} className="marginArrow" ></img></span> Crea un nuevo producto o servicio  <span>
                    <img src={Help} className="helpIcon" ></img>
                    </span>
                </p>
                <div className="widthTxt">
                <p className="txtTerceros">Para crear un nuevo producto o servicio desde tu factura, necesitas completar los siguientes tres campos . Sin embargo, si quieres crear el concepto de venta con información detallada, debes crearlo o editarlo en el módulo “Configuración” en la sección” Productos”</p>
                </div>
                <div>

                <fieldset className="radioBtn">

                    <div className="some-class">
                        <input type="radio" className="radio" name="x" value="y" id="y" />
                        <label for="y" className="labelRadio">Producto</label>
                        <input type="radio" className="radio" name="x" value="z" id="z" />
                        <label for="z" className="labelRadio">Servicio</label>
                    </div>

                </fieldset>
                
                <div className="marginRow" >
                <Row>
                    <Col>
                        <div className="marginInputCreateProductL">
                        <SearchInvoice className=" " placeholder="Ingresa el código del producto" icon="" label="Código del producto" icon_sup="" required="1" align="left"></SearchInvoice>
                        </div>
                    </Col>

                    <Col>
                    <div className="marginInputCreateProductR">
                        <SearchInvoice className=" " placeholder="Ingresa el nombre del producto" icon="" label="Nombre del producto" icon_sup="" required="1" align="left"></SearchInvoice>
                        </div>
                    </Col>
                </Row>
                </div>
                <div className="marginRow">
                <Row>
                    <Col>
                        <div className="marginInputCreateProductL">
                        <SearchInvoice className=" " placeholder="Ingresa el valor del producto" icon="" label="Valor del producto" icon_sup="" required="1" align="left"></SearchInvoice>
                        </div>
                    </Col>

                    <Col>
                        <div className="marginInputCreateProductR">
                        <SearchInvoice className=" " placeholder="Selecciona el impuesto para el producto" icon="" label="Impuesto" icon_sup="" required="0" align="left"></SearchInvoice>
                        </div>
                    </Col>
                </Row>
                </div>
                <div className="divNoMargin">
                    <p className="txtImportant">
                    *Campos obligatorios
                    </p>
                </div>

                <div>
                    <Row className="row_button_yes">
                        <Col>

                        </Col>
                        <Col className="divCreate" >
                        <button className="alerta_button button_yes">
                            Crear Producto
                        </button>
                        </Col>
                    </Row>
                    

                </div>
                
                </div>

                    {/* <img src={Alert} ></img>       
                    <p className="alerta_textBold"> ¿Estás seguro de salir sin emitir tu factura? </p>
                    <p className='alerta_text'>
                        Tu factura ha sido creada y ya tiene un consecutivo. Sin embargo, aún no la has emitido a la DIAN. Si no lo haces ahora, puedes emitirla después en el módulo de facturación para convertirla en una factura electrónica.
                        <Row className="alerta_buttons">
                            <Col lg={6}>
                                <button className="alerta_button button_cancel">
                                    Cancelar
                                </button>
                            </Col>
                            <Col lg={6}>
                                <button className="alerta_button button_yes">
                                    Si, salir
                                </button>
                            </Col>
                        </Row>  
                    </p> */}
                </Row>
            </div>
        )
    }
}

export default NewProduct;