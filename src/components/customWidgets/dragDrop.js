import React from 'react';
import { useDropzone } from 'react-dropzone';
import Check from '../../assets/icons/Check.svg'
import './../../styles/popupStyle.css'
import '../../styles/firstPageStyle.css';
import '../../styles/dragDrop.css';

function DragDropField(props) {
    const { getRootProps, getInputProps, open, acceptedFiles } = useDropzone({
        // Disable click and keydown behavior
        noClick: true,
        noKeyboard: true,
        maxFiles:1
    });

    const files = acceptedFiles.map(file => (
        file.path
    ));

    return (
        <div className="drag marginLeftSectionRuth">
            <div className="container marginDrag">
                <button className="transparentButton marginDrag" onClick={open}>
                    <div  {...getRootProps({ className: 'dropzone' })}>
                        <input {...getInputProps()} />
                        
                        <div className="uploadFile">
                            <div>
                                {files.length != 0?<img src={Check} ></img>:<img src={props.icon} ></img>}
                                {files.length != 0?
                                <p className="textUploaded">
                                {props.textUpload} <br/> <span className="uploadBold">{files}</span><br></br>
                                <span className="uploadGreen">
                                <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.233126 6.88576C0.188792 6.59526 0.166626 6.29717 0.166626 5.99967C0.166626 2.78317 2.78346 0.166342 5.99996 0.166342C9.21646 0.166341 11.8333 2.78317 11.8333 5.99967C11.8333 9.21617 9.21646 11.833 5.99996 11.833C4.17529 11.833 2.49004 11.0029 1.37588 9.55684C1.23296 9.37134 1.26796 9.10417 1.45288 8.96184C1.63838 8.81834 1.90438 8.85392 2.04846 9.03884C2.99988 10.2761 4.44071 10.9848 5.99996 10.9848C8.74863 10.9848 10.9851 8.74892 10.9851 5.99967C10.9851 3.25101 8.74863 1.01509 5.99996 1.01509C3.25129 1.01509 1.01479 3.25101 1.01479 5.99967C1.01479 6.25459 1.03404 6.50951 1.07196 6.75742C1.10696 6.98842 0.948293 7.20484 0.716709 7.24042C0.483959 7.27892 0.268709 7.11734 0.233126 6.88576ZM7.62991 6.00014C7.62991 6.11622 7.58383 6.22764 7.50099 6.30989L5.46749 8.33464C5.29599 8.50556 5.01891 8.50497 4.84858 8.33347C4.67824 8.16197 4.67824 7.88547 4.84974 7.71514L6.57233 6.00014L4.84974 4.28514C4.67824 4.11481 4.67824 3.83772 4.84858 3.66622C5.01891 3.49531 5.29599 3.49472 5.46749 3.66506L7.50099 5.69039C7.58383 5.77206 7.62991 5.88347 7.62991 6.00014Z" fill="#8DC63F"/>
                                </svg>
                                Volver a adjuntar
                                </span>
                                </p>
                                
                                :
                                <p className="textUpload">
                                {props.text}
                                </p>
                                }
                            </div>
                        </div>
                    </div>
                </button>
                
                <aside>
                    {/* <ul>{files}</ul> */}
                </aside>
            </div>
        </div>
    );
}
export default DragDropField
