import React from 'react';
import logo from '../../assets/LogoQ.svg';
import '../../App.css';
import Navigation from './navbar';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
// import InvoiceGrid from '../../components/jqwidgets/InvoiceGrid.tsx';
import ScrollView from '../../components/jqwidgets/scrollView.tsx';
import HeaderPage from './headerPage';
import Add from '../../assets/icons/Add.svg';
import SearchBar from './searchBar';
import FooterPage from './footerPage';
import '../../styles/firstPageStyle.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import BtnAdd from '../btnAdd';
import JqxScrollView from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxscrollview';
import 'jqwidgets-scripts/jqwidgets/styles/jqx.base.css';

class MainPage extends React.Component {

    constructor(props){
        super(props);
    }
    state = {
        invoiceGrid: 0,
        scrollViewPage: 0
    }
    handleCallback = (childData) => {
        console.log("childData");
        console.log(childData);
        this.setState({ invoiceGrid: childData})
    }
    buttonScrollView = (childData) => {
        console.log("childData buttonScrollView ",childData);
        this.setState({ scrollViewPage: childData})
    }
    render(){
        console.log("this.state.invoiceGrid ",this.state.invoiceGrid);
        return (
            <Container fluid className="bg_home" >
                <Row >
                    <Col lg={1} className="paddingCol">
                        <Navigation >
                        </Navigation>
                    </Col>
                    <Col lg={11} className='header_page' >
                        <HeaderPage></HeaderPage>
                        {
                            this.state.invoiceGrid.current == null?
                                ""
                            :this.state.invoiceGrid.current.props.columns.length != 0?
                                <SearchBar parentCallback = {this.buttonScrollView} invoiceGrid = {this.state.invoiceGrid}></SearchBar>
                            :null
                        }
                        {/* <Container className="table_container div_no_margin">
                <InvoiceGrid></InvoiceGrid>
            </Container> */}
                        <div className="grid-alignment-qenta invoiceGrid">
                            {/* <JqxScrollView ref={this.myScrollView} width="95%" height={360} buttonsOffset={[0, 20]} >
                                <div>
                                <InvoiceGrid parentCallback = {this.handleCallback}></InvoiceGrid>
                                </div>
                                <div><InvoiceGrid parentCallback = {this.handleCallback}></InvoiceGrid></div>
                            </JqxScrollView> */}
                            <ScrollView parentCallback = {this.handleCallback} scrollViewPage = {this.state.scrollViewPage}></ScrollView>
                        </div>
                        <Row>
                            <Container>
                                <FooterPage invoiceGrid = {this.state.invoiceGrid}></FooterPage>
                            </Container>
                            <Container>
                                <BtnAdd fromPage = {"mainPage"} invoiceGrid = {this.state.invoiceGrid}></BtnAdd>
                            </Container>
                        </Row>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default MainPage;
