import React, { Component } from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import Notification from '../../assets/icons/Bell.svg';
import Back from '../../assets/icons/Back.svg';
import Chat from '../../assets/icons/Chat.svg';
import CircleArrow from '../../assets/icons/CircleArrow.svg';
import SwitchButton from '../../components/jqwidgets/SwitchButton.tsx';
import Popup from 'reactjs-popup';
import 'bootstrap/dist/css/bootstrap.min.css';
import InvoiceAlert from './invoiceAlert';
import InfoInvoice from './infoInvoice';
import { BrowserRouter, Route, NavLink } from "react-router-dom";

class HeaderNewInvoice extends Component{
    constructor(props){
        super(props);
        this.state = {
            showModal: false,
            invoice: props.invoice,
            info: props.info,
            title_pag: props.title_pag
        }
        this.clickShowModal = this.clickShowModal.bind(this);
    }
    clickShowModal(){
        this.setState({
            showModal: !this.state.showModal
        })
    }
    render(){
        return(
            <div>
            <Row>
                <Col lg={1}  className="div_no_margin back">
                <Popup
                    trigger={<button className="transparentButton">
                        <img src={Back} ></img>
                        </button>}
                    modal
                    nested
                    closeOnDocumentClick={false}
                    closeOnEscape={false}
                >
                {close => (
                    <InvoiceAlert close = {()=>close()}/>
                    
                )}
                </Popup>
                    
                </Col>


                <Col lg={5} className="column_links">
                    <p className="first_title">
                        <NavLink className="nav_link" to="/">Facturación y cartera</NavLink> > Facturación Electrónica > {this.props.title_pag}
                    </p>
                </Col>

                <Col lg={2} className="column_auth_save">
                    <div className="auth_save">
                        Autoguardado
                        <SwitchButton/>
                    </div>
                </Col>

                <Col lg={3} className="invoice_information" >
                    <button onClick={this.clickShowModal} className="transparentButton" >
                        <img src={CircleArrow} className={!this.state.showModal?"notification_circle_arrow":"notification_circle_arrow_down"}></img>
                    </button>
                    {/* <img src={CircleArrow} className="notification_circle_arrow"></img> */}
                    <p className="notification_invoice"> <span className="eclipse"></span> Factura #</p>
                    <p className="notification_invoice_text">{this.props.invoice}</p>
                </Col>
                {
                    this.props.info ?
                        this.state.showModal ?
                            <InfoInvoice/>
                        : <div className=""></div>
                    : <div className="" style={{display:'none'}}></div>
                }

                <Col lg={1} className="notification_content" >
                    <div className="notification_ball">
                        4
                    </div> 
                    <img src={Notification}  className="notification_bell" >
                    </img> 
                </Col>
            </Row>
            
            </div>
        )
    }
}

export default HeaderNewInvoice;