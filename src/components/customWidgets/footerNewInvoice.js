import React, { Component } from 'react';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';

import Notification from '../../assets/icons/Bell.svg';
import Assistant from '../../assets/icons/Assistant.svg';
import ArrowUp from '../../assets/icons/ArrowUp.svg';
import Tooltip from './tooltip';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../../styles/footerNewInvoice.css';
import * as utils from '../../utils/utils'

import { CSSTransition } from 'react-transition-group';

class FooterNewInvoice extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            showModal: false
        }
        this.clickShowModal = this.clickShowModal.bind(this);
    }
    clickShowModal(){
        this.setState({
            showModal: !this.state.showModal
        })
    }
    render() {
        return (
            <div className="footer_invoice_container" >
                {
                    this.state.showModal ?
                        <div className="footer_values">
                            <Row className="footer_taxes">
                                <div className="footer_taxes_iva">
                                    <Row>
                                        <Col lg={7}> IVA </Col>
                                        <Col lg={5} className="aling_right"> $0 </Col>
                                    </Row>
                                    <Row>
                                        <Col lg={7}> Impuesto al consumo </Col>
                                        <Col lg={5} className="aling_right"> $0 </Col>
                                    </Row>
                                </div>
                                <div className="footer_taxes_retencion">
                                    <Row>
                                        <Col lg={7}> Rete IVA </Col>
                                        <Col lg={5} className="aling_right"> $0 </Col>
                                    </Row>
                                    <Row>
                                        <Col lg={7}> Rete ICA </Col>
                                        <Col lg={5} className="aling_right"> $0 </Col>
                                    </Row>
                                    <Row>
                                        <Col lg={7}> Rete fuente </Col>
                                        <Col lg={5} className="aling_right"> $0 </Col>
                                    </Row>
                                </div>
                            </Row>
                        </div>
                    : <div className="footer_values"></div>
                }
                <Row>
                    <Col lg={1} className="img_assistant" >
                        <img src={Assistant} ></img>
                    </Col>
                    <Col lg={4} >
                        <p className='footerText'>
                            <span className="footerTextBold"> Ayudante: </span>
                            ¡Sácale el jugo al módulo de facturación electrónica de Q·enta! Conoce en el siguiente video tutorial todo lo que puedes hacer.
                            <span className="footerTextBlue"> ¡Míralo aquí!: </span>
                        </p>
                        
                    </Col>
                    <Col lg={6} >
                        <button onClick={this.clickShowModal} className="transparentButton" >
                            <span className="footerCircle tooltip"><img src={ArrowUp} className={!this.state.showModal?"arrow_left":"arrow_right"}></img><Tooltip rotate={90} content={"Detalle de impuestos"}/></span>
                        </button>
                        <div className='footerValues'>
                            <Row>
                                <Col lg={10}> Subtotal </Col>
                                <Col lg={2} className="aling_right"> $0 </Col>
                            </Row>
                            <Row>
                                <Col lg={10}> Descuento </Col>
                                <Col lg={2} className="aling_right"> $0 </Col>
                            </Row>
                            <Row>
                                <Col lg={10}> Impuestos </Col>
                                <Col lg={2} className="aling_right"> $0 </Col>
                            </Row>
                            <Row className='footerTotal'>
                                <Col lg={6}> Total a pagar </Col>
                                <Col lg={6} className="aling_right"> $999.000.000,00 </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
                
            </div>
        )
    }
}

export default FooterNewInvoice;