import React, { Component } from 'react';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';

import Notification from '../../assets/icons/Bell.svg';
// import Back from './assets/icons/Back.svg';
import Go from '../../assets/icons/Go.svg';
import Search from '../../assets/icons/Search.svg';
import Help from '../../assets/icons/Help.svg';

import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';

import Important from './important';

import 'bootstrap/dist/css/bootstrap.min.css';


class SearchBar extends Component {
    constructor(props){
        super(props);
        this.state = {
            invoiceGrid: props.invoiceGrid,
            sliderPage: 0
        }
        this.buttonScrollView = this.buttonScrollView.bind(this);
    }
    componentWillReceiveProps(nextProps){
        console.log("componentWillReceiveProps in footer Page");
        console.log(nextProps);
        // console.log("nextProps ",nextProps.invoiceGrid.current.props.columns.length);
        this.setState({
            invoiceGrid: nextProps.invoiceGrid,
            sliderPage: nextProps.sliderPage
        })
    }
    buttonScrollView(){
        if(this.state.sliderPage == 0)
            this.state.sliderPage = 1;
        else    
            this.state.sliderPage = 0;
        console.log("buttonScrollView ",this.state.sliderPage);
        this.props.parentCallback(this.state.sliderPage);
    }
    render() {
        // try {
            // if(this.props.invoiceGrid.current.props.columns.length != 0){
                return (
                    <div className="search_bar" >
        
                        <Row>
                            <Col lg={1} className="divNoMargin width_back" >
                            </Col>
                            <Col lg={4} >
                                <InputGroup size="sm"  >
                                    <FormControl aria-label="Text input with checkbox" placeholder="Buscar por cliente, documento o consecutivo" className="input_form" />
                                    <InputGroup.Text className="input_suffix">
                                    <img src={Search} className="icon_search" >
        
                                    </img>
        
                                    </InputGroup.Text>
                                </InputGroup>
                            </Col>
        
                            <Col lg={1} className="divNoMargin" >
                                <div className="contain_icon_help">
                                    <img src={Help} className="icon_help" >
                                    </img>
                                    <Important />
                                </div>
                            </Col>
                            <Col lg={6} className="go_to_section">
                                <button onClick={this.buttonScrollView} className="transparentButton">
                                    <p className="go_to_section_credit">
                                    Ir a notas débito y crédito  &nbsp;  &nbsp; <span>
                                    <img src={Go}  >
                                    </img>
                                    </span>
                                    </p>
                                </button>
                            </Col>
        
        
        
                        </Row>
                    </div>
                )
        //     }
        // } catch (error) {
        //     return ("");
        // }
        
    }
}

export default SearchBar;