import React, { Component } from 'react';

class Tooltip extends Component{
    constructor(props){
        super(props);
        this.setState({
            content: props.content,
            rotate: props.rotate
        });
    }

    render(){
        return(<span style={{transform: "rotate("+this.props.rotate+"deg)"}} className="tooltiptext">{this.props.content}</span>)
    }
}

export default Tooltip