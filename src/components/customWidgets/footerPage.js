import React, { Component } from 'react';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';

import Notification from '../../assets/icons/Bell.svg';
// import Back from './assets/icons/Back.svg';
import Go from '../../assets/icons/Go.svg';
import Help from '../../assets/icons/Help.svg';
import Assistant from '../../assets/icons/Assistant.svg';
import Arrow from '../../assets/icons/Arrow.svg';
import BtnAdd from '../btnAdd';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Icon from './Icon'

import 'bootstrap/dist/css/bootstrap.min.css';


class FooterPage extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            invoiceGrid: props.invoiceGrid
        }
    }
    componentWillReceiveProps(nextProps){
        console.log("componentWillReceiveProps in footer Page");
        // console.log("nextProps ",nextProps.invoiceGrid.current.props.columns.length);
        this.setState({
            invoiceGrid: nextProps
        })
    }
    render() {
        try {
            if(this.props.invoiceGrid.current.props.columns.length != 0){
                return (
                    <div className="footer_container" >
                        <Row>
                            <Col lg={2} className="img_assistant" >
                                <img src={Assistant}  >
                                </img>
                                {/* <Icon fill="#00ff00" className="myclass" /> */}
        
                            </Col>
                            <Col lg={10} >
                                <p className='footerText'>
                                    <span className="footerTextBold"> Ayudante: </span>
                                    Aquí y al instante puedes conocer si tu factura está lista para emitir, descargar el XML o la representación gráfica luego de la emisión, y verificar si tu cliente ya aceptó tu factura. Además, en el botón <span className="paragraph-bold"> + </span> podrás elegir entre crear una factura, una nota débito o una nota crédito.
                                </p>
                                
                            </Col>
                        </Row>
                    </div>
                )
            }
        } catch (error) {
            return (<Container className="table_empty">
            <p className="title_table_empty table_empty_footer">Crea aquí tu primera factura <img src={Arrow}></img><br></br></p></Container>)
        }
        // }else{return null}
    }
}

export default FooterPage;