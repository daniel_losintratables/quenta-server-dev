import React, { Component } from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import '../../styles/important.css';
import Importante from '../../assets/icons/Importante.svg';


class Important extends Component {
    render() {
        return(
            <div className="contain_important">
                <div className="arrow_important">
                </div>
                <div className="section_important">
                    <div className="important_header">
                        <img src={Importante} ></img>
                        <p className="important_titl">
                            Importante!
                        </p>
                    </div>
                    <p className="subtitle">
                        <b>Conoce aquí cada uno de los estados posibles en tus facturas de venta:</b>
                    </p>
                    <p className="text">
                        <b>Borrador:</b> Aún no has terminado la creación de tu factura y, por lo tanto, no tiene asignado un consecutivo. <br/>
                        <b>Pendiente de pago:</b> Tu factura no ha vencido y no se ha pagado al 100%. 
                    </p>
                </div>
                
            </div>
        )
    }
}

export default Important;