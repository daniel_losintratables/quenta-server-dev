import React, { Component } from 'react';
import './../../styles/popupStyle.css'
import './../../styles/newHeaderField.css'

import 'bootstrap/dist/css/bootstrap.min.css';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Back from '../../assets/icons/Back.svg';
import ArrowLeft from '../../assets/icons/ArrowLeft.svg'
import { Container } from 'react-bootstrap';
import SearchBar from './searchBar';
import SearchInvoice from './quentaInput';
import ArrowDown from '../../assets/icons/ArrowDown.svg';
import Help from '../../assets/icons/Help.svg';
import Calendar from '../../assets/icons/Calendar.svg';
import UploadCircle from '../../assets/icons/UploadCircle.svg';
import Coment from '../../assets/icons/Coment.svg';
import Plus from '../../assets/icons/Plus.svg';
import Note from '../../assets/icons/Note.svg';
import DragDropField from '../customWidgets/dragDrop'
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import DateInput from '../jqwidgets/DateInput';
import Combobox from '../jqwidgets/Combobox';
class NewHeaderField extends Component{
    content = ["Sucursal 1","Sucursal 2", "Sucursal 3", "Sucursal 4", "Sucursal 5"]
    constructor(props){
        super(props)
        this.state = {
            radioValue: ""
        }
        this.selectRadio = this.selectRadio.bind(this);
    }
    selectRadio(event){
        this.setState({
            radioValue: event.target.value
        });
        console.log(this.state.radioValue);
    }
    render(){
        return(
        <div className="newHeaderField popBody">
        <Row className="divNoMargin bgBalls">
            <Col lg={12} md={12} className="fullHeightCol">
                <p className="titleNewClient">
                    <span>  <img src={ArrowLeft} className="marginArrow" ></img></span>
                </p>
                <div className="container_newheaderfield">
                <p className="txtTerceros">Si necesitas cambiar la configuración predeterminada de tu factura, hazlo fácilmente. <br/>
                    Recuerda que estos ajustes quedarán guardados para próximas facturas y puedes cambiarlos cuantas veces quieras.</p>

                <Row className="divNoMargin row_headerfield">
                    <Col lg={5} className="divNoMargin ">
                        <div className="txtTercerosInput">
                        <SearchInvoice placeholder="Ingresa el correo electrónico aquí" icon="" label="Enviar factura a un correo específico" icon_sup={Help} required="0"  ></SearchInvoice>
                        </div>
                    </Col>
                    <Col lg={2} className="divNoMargin ">
                        <div className="txtTercerosInput">
                        {/* <SearchInvoice placeholder="" icon={ArrowDown} label="Sucursal para facturar" icon_sup={Help} required="0" align="center" ></SearchInvoice> */}
                        <Combobox placeholder="" label="Sucursal para facturar" icon_sup={Help} required="0" icon="ArrowDown" content={this.content} align="combo_center" width = {"185px"} height = {"25"}/>
                        </div>
                    </Col>
                    <Col lg={2} className="divNoMargin ">
                        <div className="txtTercerosInput">
                        {/* <SearchInvoice placeholder="" icon={ArrowDown} label="Bodega asignada" icon_sup={Help} required="0" align="center" ></SearchInvoice> */}
                        <Combobox placeholder="" label="Bodega asignada" icon_sup={Help} required="0" icon="ArrowDown" content={["Bodega 1", "Bodega 2"]} align="combo_center" width = {"185px"} height = {"25"}/>
                        </div>
                    </Col>
                    <Col lg={2} className="divNoMargin ">
                        <div className="txtTercerosInput">
                        {/* <SearchInvoice placeholder="" icon={ArrowDown} label="Vendedor | Comercial" icon_sup={Help} required="0" align="center" ></SearchInvoice> */}
                        <Combobox placeholder="" label="Vendedor | Comercial" icon_sup={Help} required="0" icon="ArrowDown" content={["Vendedor 1", "Vendedor 2"]} align="combo_center" width = {"185px"} height = {"25"}/>
                        </div>
                    </Col>
                </Row>

                <Row className="divNoMargin" onChange={this.selectRadio}>
                    <Col lg={8} className="divNoMargin check_section">
                        <p className="txtCheckBox firstCheckBox">
                            Tipo de Factura: <img className="icon_search" src={Help} ></img>
                        </p>
                        <div className="circleCheckBox firstCheckBox">
                            <input type="radio" value="estandar" name="gender" />Estándar
                        </div>
                        <div className="circleCheckBox firstCheckBox">
                            <input type="radio" value="AIU" name="gender" />AIU
                        </div>
                        <div className="circleCheckBox firstCheckBox">
                            <input type="radio" value="salud" name="gender" />Sector salud
                        </div>
                    </Col>
                </Row>

                <p className="txtTerceros">Si tu cliente necesita que relaciones una orden de compra en la factura, este es el espacio para hacerlo.</p>

                <Row className="row_infoSection divNoMargin" >
                    <Col lg={5} className="newHeaderField_infoSection">
                        <p className="titleNewClient">
                            Orden de servicio
                        </p>
                        <Row className="divNoMargin" >
                            <Col lg={7} className="divNoMargin ">
                                <div className="txtTercerosInput">
                                <SearchInvoice placeholder="" icon="" label="Número" icon_sup={Help} required="0"  ></SearchInvoice>
                                </div>
                            </Col>
                            <Col lg={5} className="divNoMargin ">
                                <div className="txtTercerosInput">
                                {/* <SearchInvoice placeholder="" icon={Calendar} label="Fecha" icon_sup={Help} required="0"  ></SearchInvoice> */}
                                <DateInput placeholder="" icon={Calendar} label="Fecha" icon_sup={Help} required="0" align="center"/>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                    <Col lg={5} className="newHeaderField_infoSection">
                        <p className="titleNewClient">
                            Orden de recepción
                        </p>
                        <Row className="divNoMargin" >
                            <Col lg={7} className="divNoMargin ">
                                <div className="txtTercerosInput">
                                <SearchInvoice placeholder="" icon="" label="Número" icon_sup={Help} required="0"  ></SearchInvoice>
                                </div>
                            </Col>
                            <Col lg={5} className="divNoMargin ">
                                <div className="txtTercerosInput">
                                {/* <SearchInvoice placeholder="" icon={Calendar} label="Fecha" icon_sup={Help} required="0"  ></SearchInvoice> */}
                                <DateInput placeholder="" icon={Calendar} label="Fecha" icon_sup={Help} required="0" align="center"/>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row className="row_infoSection divNoMargin" >
                    <Col lg={6} className="newHeaderField_title">
                        <div className="txtOption">
                            <p  className="divNoMargin">
                            ICA
                            </p>
                        </div>
                    </Col>
                    {this.state.radioValue == "salud"?
                        <Col lg={6} className="newHeaderField_title">
                        <div className="txtOption">
                            <p  className="divNoMargin">
                            <span> <img className="marginIcon" src={Note} ></img></span>Plantilla Sector Salud 
                            </p>
                        </div>
                    </Col>
                    :
                    <Col lg={6} className="newHeaderField_title">
                        
                    </Col>
                    }
                    
                </Row>

                <Row className="row_infoSection divNoMargin" >
                    <Col lg={5} className="newHeaderField_infoSection">
                        <p className="titleNewClient">
                            Impuesto de Industria y Comercio
                        </p>
                        <Row className="divNoMargin" >
                            <Col lg={7} className="divNoMargin ">
                                <div className="txtTercerosInput">
                                <SearchInvoice placeholder="" icon="" label="Lugar de negociación" icon_sup={Help} required="0"  ></SearchInvoice>
                                </div>
                            </Col>
                            <Col lg={5} className="divNoMargin ">
                                <div className="txtTercerosInput">
                                <SearchInvoice placeholder="" icon={Calendar} label="Tarifa" icon_sup={Help} required="0"  ></SearchInvoice>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                    {this.state.radioValue == "salud"?
                    <Col lg={5}>
                        <DragDropField icon={UploadCircle} text="Arrastra o haz clic para cargar tu plantilla del sector salud" textUpload="Archivo adjunto:"/>
                    </Col>
                    :
                    <Col lg={5}>
                    </Col>
                    }
                </Row>

                <Row className="row_infoSection divNoMargin" >
                    <Col lg={7}>
                        <div className="txtOption">
                            <p  className="divNoMargin">
                                <img src={Coment}></img> Comentarios | Observaciones
                            </p>
                        </div>
                    </Col>
                    <Col lg={4}>
                        <div className="txtOption">
                            <p  className="divNoMargin">
                            <span> <img className="marginIcon" src={Note} ></img></span>Archivos adjuntos
                            </p>
                        </div>
                    </Col>
                </Row>
                <Row className="row_comments divNoMargin" >
                    <Col lg={7}>
                        <div className="input_comments">
                            <InputGroup size="sm" className="inputHeigth" >
                                <FormControl as="textarea" aria-label="Text input with checkbox" placeholder="Ingresa aquí los comentarios u observaciones que necesitas comunicar a tu cliente como condiciones de envío, números de cuentas bancarias para pagos, entre otros." className="input_form" />
                            </InputGroup>
                        </div>
                    </Col>
                    <Col lg={4} className="addFile">
                        <DragDropField icon={UploadCircle} text="Arrastra o haz clic aquí para cargar un archivo adjunto." textUpload="Archivo adjunto:"/>
                    </Col>
                </Row>
                </div>
            </Col>
        </Row>
        
        </div>
        
        )
    }
}

export default NewHeaderField;
