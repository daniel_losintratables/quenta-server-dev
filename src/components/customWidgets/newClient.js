import React, { Component } from 'react';
import './../../styles/popupStyle.css'

import 'bootstrap/dist/css/bootstrap.min.css';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Back from '../../assets/icons/Back.svg';
import ArrowLeft from '../../assets/icons/ArrowLeft.svg'
import { Container } from 'react-bootstrap';
import SearchBar from './searchBar';
import SearchInvoice from './quentaInput';
import Help from '../../assets/icons/Help.svg';
import Plus from '../../assets/icons/Plus.svg';
import Upload from '../../assets/icons/Upload.svg'
import DragDropField from '../customWidgets/dragDrop'
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
class NewClient extends Component{
    render(){
        return(
          <div className="popBody">
          <Row className="divNoMargin bgBalls">
            
              <Col lg={8} md={8} className="fullHeightCol">               
                <p className="titleNewClient pointer" onClick={()=>this.props.close()}   >
                    <span>  <img src={ArrowLeft} className="marginArrow" ></img></span> Crea un nuevo cliente
                </p>
                <p className="txtTerceros">Para crear un nuevo tercero desde tu factura, necesitas completar los siguientes campos . Sin embargo, si <br></br> quieres crear un tercero con información más detallada, puedes crearlo o editarlo en el módulo <br></br> “Configuración” en la sección” Terceros”</p>

                <Row className="divNoMargin" >
                  <Col lg={5} className="divNoMargin ">
                  <div className="txtTercerosInput">
                  <SearchInvoice placeholder="Selecciona el tipo de documento" icon="" label="Tipo de documento*" icon_sup="" required="0"  ></SearchInvoice>
                  </div>
                  </Col>
                  <Col lg={5} className="divNoMargin ">
                  <div className="txtTercerosInput">
                  <SearchInvoice placeholder="Escribe el número del documento" icon="" label="Número de documento*" icon_sup="" required="0"  ></SearchInvoice>
                  </div>
                  </Col>
                </Row>

                <Row className="divNoMargin" >
                  <Col lg={5} className="divNoMargin ">
                  <div className="txtTercerosInput">
                  <SearchInvoice placeholder="Escribe el nombre o razón social de tu cliente" icon="" label="Nombre completo | Razón social*" icon_sup="" required="0"  ></SearchInvoice>
                  </div>
                  </Col>
                  <Col lg={5} className="divNoMargin ">
                  <div className="txtTercerosInput">
                  <SearchInvoice placeholder="Escribe el correo electrónico" icon="" label="Correo electrónico*" icon_sup="" required="0"  ></SearchInvoice>
                  </div>
                  </Col>
                </Row>

                

                <Row className="divNoMargin" >
                  <Col lg={4} className="divNoMargin ">
                  <div className="divNoMargin">
                  <p className="txtImportant">
                  *Campos obligatorios
                  </p>
                </div>
                  <div className="txtOption">
                    <p  className="divNoMargin">
                    Datos opcionales <span> <img className="marginIcon" src={Plus}  ></img></span>
                    </p>
                  </div>
                  </Col>

                
                  <Col lg={1}>
                    
                  </Col>
                  <Col lg={5} className="divNoMargin btnCrearProducto"  >
                  <button className="alerta_button button_yes createClient">
                            Crear Cliente
                        </button>
                  </Col>
                  

                  
                </Row>
                <Row className="divNoMargin" >
                  <Col lg={5} className="divNoMargin ">
                  <div className="marginInputNC">
                  <SearchInvoice placeholder="Escribe un número de teléfono" icon="" label="Teléfono" icon_sup="" required="0"  ></SearchInvoice>
                  </div>
                  </Col>
                  <Col lg={5} className="divNoMargin ">
                  <div className="marginInputNC">
                  <SearchInvoice placeholder="Selecciona el tipo de persona " icon="" icon_sup={Help} label="Tipo de persona"  required="0"  ></SearchInvoice>
                  </div>
                  </Col>
                </Row>


                <Row className="divNoMargin" >
                  <Col lg={5} className="divNoMargin ">
                  <div className="marginInputNC">
                  <SearchInvoice  placeholder="Escribe la dirección de tu cliente " icon="" label="Dirección" icon_sup="" required="0"  ></SearchInvoice>
                  </div>
                  </Col>
                  <Col lg={5} className="divNoMargin ">
                  <div className="marginInputNC">
                  <SearchInvoice placeholder="Selecciona el tipo de régimen" icon="" icon_sup="" label="Tipo de régimen"  required="0"  ></SearchInvoice>
                  </div>
                  </Col>
                </Row>


                <Row className="divNoMargin" >
                  <Col lg={5} className="divNoMargin ">
                  <div className="marginInputNC">
                  <SearchInvoice  placeholder="Torre, entrada, bloque, conjunto residencial... " icon="" label="Dirección (Datos complementarios)" icon_sup="" required="0"  ></SearchInvoice>
                  </div>
                  <div className="marginInputNC">
                  <SearchInvoice  placeholder="Selecciona el país de esta dirección " icon="" label="País" icon_sup="" required="0"  ></SearchInvoice>
                  </div>
                  <div className="marginInputNC">
                  <SearchInvoice  placeholder="Selecciona la ciudad de esta dirección " icon="" label="Ciudad" icon_sup="" required="0"  ></SearchInvoice>
                  </div>
                  </Col>
                
                  

                
                  <Col lg={5} className="divNoMargin ">
                  <div className="marginInputNC">
                  <Row className="divNoMargin">
                    <Col lg={11} className="divNoMargin">
                      <p className="txtCheckBox firstCheckBox ">
                      Gran contribuyente
                      </p>
                    </Col>
                    <Col className="divNoMargin ">
                      <div className="circleCheckBox firstCheckBox">
                    <input type="radio" value="Male" name="gender" /> 
                    </div>
                    </Col>

                  </Row>
                  <Row className="divNoMargin">
                    <Col lg={11} className="divNoMargin">
                      <p className="txtCheckBox">
                      Autorretenedor ICA
                      </p>
                    </Col>
                    <Col className="divNoMargin">
                    <div className="circleCheckBox">
                    <input type="radio" value="Male" name="gender" /> 
                    </div>
                    </Col>

                  </Row>
                  <Row className="divNoMargin">
                    <Col lg={11} className="divNoMargin">
                      <p className="txtCheckBox">
                      Autorretenedor renta
                      </p>
                    </Col>
                    <Col className="divNoMargin">
                    <div className="circleCheckBox">
                    <input type="radio" value="Male" name="gender" /> 
                    </div>
                    </Col>

                  </Row>
                  <Row className="divNoMargin">
                    <Col lg={11} className="divNoMargin">
                      <p className="txtCheckBox">
                      Responsable de IVA
                      </p>
                    </Col>
                    <Col className="divNoMargin">
                    <div className="circleCheckBox">
                    <input type="radio" value="Male" name="gender" /> 
                    </div>
                    </Col>

                  </Row><Row className="divNoMargin">
                    <Col lg={11} className="divNoMargin">
                      <p className="txtCheckBox">
                      Responsable de impuesto al consumo
                      </p>
                    </Col>
                    <Col className="divNoMargin">
                    <div className="circleCheckBox">
                    <input type="radio" value="Male" name="gender" /> 
                    </div>
                    </Col>

                  </Row>
                  </div>
                  </Col>
                </Row>

                

              </Col>
              <Col lg={4} md={4} className="divNoMargin rowRight fullHeightCol radiusRow">
              <div className="sectionRut">
                <p className="titleRutSemiBold" >¡Crea tu cliente más rápido  <br></br>
                con  <span className="underline"> MandaTuRut!</span> <br></br>
                <span className="titleRutSemiMedium">
                Carga el RUT y agiliza el proceso <br></br> de creación. </span>
                </p>
                </div>
                
                {/* <div className="drag marginLeftSectionRuth"> */}
                <DragDropField icon={Upload} text="Arrastra el archivo .pdf o haz clic aquí para cargar el RUT de tu cliente." textUpload="Se adjuntó correctamente el archivo:"/>
                {/* </div> */}

                <div className="divNoMargin" >


                <p className="titleRutSemiMedium marginLeftSectionRuth ">
                o ingresa la URL generada en el <br></br> app de <span className="underline" > MandaTuRut. </span> </p>

                
                


                </div>

              
                <div className="input-group marginRightSectionRuth  ">
                    <input type="text" className="form-control inputUrl" placeholder="Ingresa la URL del rut aquí" aria-label="Text input with radio button"></input> 
                </div>
                
                <div className="disclaimerText">
                  <p className="titleImportant"  >
                  Importante:
                  </p>
                
                  Ten en cuenta que el RUT debe ser el descargado en el sitio web de la DIAN. Si es una fotocopia o una imagen escaneada el sistema lo detectará como no válido.

                  
                  Conoce más en: www.mandaturut.com 
                
                </div>
                
              </Col>
          </Row>
          
          </div>
          
        )
    }
}

export default NewClient;
