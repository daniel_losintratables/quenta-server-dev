import React, { Component } from 'react';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';

import Notification from '../../assets/icons/Bell.svg';
// import Back from './assets/icons/Back.svg';
import Back from '../../assets/icons/Back.svg';
import Chat from '../../assets/icons/Chat.svg';


import 'bootstrap/dist/css/bootstrap.min.css';
class HeaderPage extends Component{
    render(){
        return(
            <div>
            <Row>
            <Col lg={1}  className="divNoMargin back">
              <img src={Back} ></img>
            </Col>


            <Col lg={5}>
              <p className="first_title">
                Facturación y cartera
              </p>
              <p className="second_title">
                Facturas electrónicas.
              </p>
            </Col>

            <Col lg={5} className="personal_data_section">
              <p className="welcome_name">
                Bienvenido, Alejandro
              </p>
              <p className="company_view">
                Estás viendo: Globalsoft Wigilabs SAS
              </p>
              <p className="txt_date">
                Lunes, 10 de septiembre de 2019
              </p>
              <p className="help">
                <span>
                  <img src={Chat} >
                  </img>

                </span>
                ¿Necesitas ayuda?
              </p>
            </Col>

            <Col lg={1} className="notification_content" >
            <div className="notification_ball">

                4
                </div> 
            <img src={Notification}  className="notification_bell" >
                        </img> 

                        

              

            </Col>

          </Row>
          </div>
        )
    }
}

export default HeaderPage;