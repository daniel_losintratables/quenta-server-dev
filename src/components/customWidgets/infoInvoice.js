import React, { Component } from 'react';

import '../../styles/infoInvoice.css';

import State from '../../assets/icons/State.svg';


class InfoInvoice extends Component {
    render() {
        return(
            <div className="contain_info">
                <div className="row_info state_info">
                    <div className="title_info">Estado: </div>
                    <div className="text_info text_eclipse"> <span className="eclipse"></span>Pago total</div>
                </div>
                <div className="row_info">
                    <div className="contain_text_info">
                        <div className="title_info">Cotización: </div>
                        <div className="text_info">COT23001000</div>

                        <div className="title_info">Notas débito asociadas: </div>
                        <div className="text_info">Si <a href="#">Ver</a> </div>

                        <div className="title_info">Orden de compra: </div>
                        <div className="text_info">OP001000456</div>
                    </div>
                    <div className="contain_text_info contain_text_margin">
                        <div className="title_info">Orden de pedido: </div>
                        <div className="text_info">OP001000456</div>

                        <div className="title_info">Notas crédito asociadas: </div>
                        <div className="text_info">Si <a href="#">Ver</a></div>

                        <div className="title_info">Estado recepción: </div>
                        <div className="text_info"><img src={State}></img></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default InfoInvoice;