import React, { Component } from 'react';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import Alert from '../../assets/icons/Alert.svg';
import '../../styles/invoiceAlert.css';
import * as utils from '../../utils/utils';
import { BrowserRouter, Route, NavLink } from "react-router-dom";

class InvoiceAlert extends Component {
    constructor(props){
        super(props);
    }
    closePopUp(){
        // this.props.co
    }
    render() {
        return (
            <div className="alerta_popup">
                <Row>
                    <img src={Alert} ></img>       
                    <p className="alerta_textBold"> ¿Estás seguro de salir sin emitir tu factura? </p>
                    <p className='alerta_text'>
                        Tu factura ha sido creada y ya tiene un consecutivo. Sin embargo, aún no la has emitido a la DIAN. Si no lo haces ahora, puedes emitirla después en el módulo de facturación para convertirla en una factura electrónica.
                        <Row className="alerta_buttons">
                            <Col lg={6}>
                                <button className="alerta_button button_cancel" onClick={()=>this.props.close()}>
                                    Cancelar
                                </button>
                            </Col>
                            <Col lg={6}>
                                <NavLink to="/">
                                    <button className="alerta_button button_yes">
                                        Si, salir
                                    </button>
                                </NavLink>
                            </Col>
                        </Row>  
                    </p>
                </Row>
            </div>
        )
    }
}

export default InvoiceAlert;