import React, { Component } from 'react';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';

import Notification from '../../assets/icons/Bell.svg';
// import Back from './assets/icons/Back.svg';
import Go from '../../assets/icons/Go.svg';
import Search from '../../assets/icons/Search.svg';
import Help from '../../assets/icons/Help.svg';

import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';

import Important from './important';

import 'bootstrap/dist/css/bootstrap.min.css';

import '../../styles/quentaInput.css';



class QuentaInput extends Component {
    constructor(props){
        super(props);
        this.state = {
            class: props.class,
            invoiceGrid: props.invoiceGrid,
            placeholder: props.placeholder,
            icon: props.icon,
            label: props.label,
            icon_sup: props.icon_sup,
            required: props.required,
            align: props.align,
        }
    }
    componentWillReceiveProps(nextProps){
        console.log("componentWillReceiveProps in footer Page");
        // console.log("nextProps ",nextProps.invoiceGrid.current.props.columns.length);
        this.setState({
            invoiceGrid: nextProps
        })
    }
    render() {
        var text_style = "";
        var required = "";
        if (this.props.icon!="") {
            var icono = <img src={this.props.icon} className="icon_search" ></img>
        }
        if (this.props.icon_sup!="") {
            var icono_sup = <div className="contain_icon_help"><img src={this.props.icon_sup} className="icon_search" ></img> <Important /></div>
        }
        if (this.props.required==1){
            required = "*";
        }
        return (
            <div className={this.props.class} >
                <Row>
                    <Col lg={12} >
                        <div className="title_search">{this.props.label}{required} {icono_sup}</div> 
                        <InputGroup size="sm" className="inputHeigth" >
                            <FormControl aria-label="Text input with checkbox" placeholder={this.props.placeholder} className="input_form" style={{textAlign:this.props.align}} />
                            <InputGroup.Text className="input_suffix">
                                {icono}
                            </InputGroup.Text>
                        </InputGroup>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default QuentaInput;