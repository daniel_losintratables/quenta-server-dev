import React from 'react';
import '../../App.css';
import Navigation from './navbar';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import CheckInvoiceGrid from '../../components/jqwidgets/CheckInvoiceGrid.tsx';
import HeaderNewInvoice from './headerNewInvoice';
import AddGris from '../../assets/icons/AddGris.svg';
import FooterNewInvoice from './footerNewInvoice';
import '../../styles/firstPageStyle.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import BtnAdd from '../btnAdd';
import 'jqwidgets-scripts/jqwidgets/styles/jqx.base.css';

import Calendar from '../../assets/icons/Calendar.svg';
import Help from '../../assets/icons/Help.svg';

import '../../styles/newInvoicePage.css';

import Popup from 'reactjs-popup';
import NewHeaderField from './newHeaderField';
import DateInput from '../jqwidgets/DateInput';
import Combobox from '../jqwidgets/Combobox';
import NewClient from './newClient';
import Drive from '../../assets/icons/Drive.svg';
import Data from '../../assets/icons/Data.svg';

class CheckInvoicePage extends React.Component {
    content = [
        "<div class='add_button_combo'> <div><img src='" + Data + "'}></img></div>Consumidor final</div>", 
        "<div class='add_button_combo'> <div><img src='" + Drive + "'}></img></div>Santiago Chiriboga</div>",
        "<div class='add_button_combo'> <div><img src='" + Data + "'}></img></div>Inti Guzman</div>",
        "<div class='add_button_combo'> <div><img src='" + Data + "'}></img></div>Daniela Gallegos</div>",
        "<div class='add_button_combo'> <div><img src='" + Drive + "'}></img></div>Cristhian Acosta</div>",
        "<div class='add_button_combo'> <div><img src='" + Data + "'}></img></div>Daniel Chiluisa</div>"]
    content_payment_way = ["Efectivo","Tarjeta débito","Tarjeta crédito","Transferencia electrónica","Consignación","Billetera electrónica","Cheque"]
    content_payment_form = ["Contado","Crédito a 30 días","Crédito a 45 días","Crédito a 60 días","Crédito a 90 días","Personalizada"]
    state = {
        invoiceGrid: 2
    }
    handleCallback = (childData) => {
        console.log("childData");
        console.log(childData);
        this.setState({ invoiceGrid: childData})
    }
    showCreditTable= (childData) => {
        console.log("childData");
        console.log(childData);
        // this.setState({ invoiceGrid: childData})
    }

    constructor(props){
        super(props);
        let newClientPopup = false
    }
    clientPopup = (open) => {
        console.log("open");
        console.log(open);
        this.setState({ newClientPopup: open})
    }
    render(){
    return (
        <Container fluid className="bg_home" >
            <Popup open={this.state.newClientPopup}
                modal
                nested
                // onOpen={()=>{this.setState({newClientPopup: false});}}
            >
            {close => (
                <NewClient close = {()=>close()}></NewClient>
            )}
            </Popup>
            <Row >
                <Col lg={1} className="paddingCol">
                    <Navigation >
                    </Navigation>
                </Col>
                <Col lg={11} className='header_page' >
                    <HeaderNewInvoice invoice="FDV0023" info={true} title_pag="Consulta factura">
                    </HeaderNewInvoice>
                    <Row className="row_search">
                        <Col lg={4} className="divNoMargin">
                            <div className="txtInputNewInvoice">
                                <Combobox class="search_invoice search_bar" placeholder="   Ingresa el nombre del cliente" label="Cliente" icon_sup={Help} required="1" icon="" content={this.content} width = {"400px"} height = {"25"} comboType="Cliente" open={this.clientPopup}/>
                                {/* <SearchInvoice class="search_invoice search_bar" invoiceGrid = {this.state.invoiceGrid} placeholder="Ingresa el nombre del cliente" icon="" label="Cliente" icon_sup={Help} required="1" ></SearchInvoice> */}
                            </div>
                        </Col>
                        <Col lg={2} className="divNoMargin">
                            <div className="txtInputNewInvoice">
                                {/* <SearchInvoice class="search_invoice search_bar" invoiceGrid = {this.state.invoiceGrid} placeholder="" icon={Calendar} label="Fecha de facturación" icon_sup={Help} required="1" align="center"></SearchInvoice> */}
                                <DateInput class="search_invoice search_bar" placeholder="" icon={Calendar} label="Fecha de facturación" icon_sup={Help} required="1" align="center"/>
                            </div>
                        </Col>
                        <Col lg={2} className="divNoMargin">
                            <div className="txtInputNewInvoice">
                            <Combobox class="search_invoice search_bar" placeholder="" label="Medio de pago" icon_sup={Help} required="0" icon="" content={this.content_payment_way} align="combo_center" width = {"185px"} height = {"25"} />
                                {/* <QuentaInput class="search_invoice search_bar" invoiceGrid = {this.state.invoiceGrid} placeholder="" icon={ArrowDown} label="Medio de pago" icon_sup={Help} required="0" align="center"></QuentaInput> */}
                            </div>
                        </Col>
                        <Col lg={2} className="divNoMargin">
                            <div className="txtInputNewInvoice">
                            <Combobox class="search_invoice search_bar" placeholder="" label="Forma de pago" icon_sup={Help} required="0" icon="" content={this.content_payment_form} align="combo_center" width = {"185px"} height = {"25"}/>
                                {/* <QuentaInput class="search_invoice search_bar" invoiceGrid = {this.state.invoiceGrid} placeholder="" icon={ArrowDown} label="Forma de pago" icon_sup={Help} required="1" align="center"></QuentaInput> */}
                            </div>
                        </Col>
                        <Col lg={2}>
                            <Popup className="popup_newHeaderField"
                                trigger={<button className="transparentButton add_button">
                                    <img src={AddGris}></img>
                                </button>}
                                modal
                                nested
                            >
                            {close => (
                                <NewHeaderField></NewHeaderField>
                            )}
                            </Popup>
                        </Col>
                    </Row >
                    <div className="grid-alignment-qenta">
                        <div className="grid_newInvoiceGrid newInvoiceGrid">
                            <CheckInvoiceGrid />
                        </div>
                    </div>
                    <Row>
                        <Container>
                            <FooterNewInvoice invoiceGrid = {this.state.invoiceGrid}></FooterNewInvoice>
                        </Container>
                        <Container>
                            <BtnAdd fromPage = {"checkInvoice"} invoiceGrid = {this.state.invoiceGrid} ></BtnAdd>
                        </Container>
                    </Row>
                    {/* <Popup
                        trigger={true}
                        modal
                        nested
                    >
                    {close => (
                        <NewClient></NewClient>
                    )}
                    </Popup> */}
                </Col>
            </Row>
        </Container>
    );
    }
}

export default CheckInvoicePage;
