import * as React from 'react';
import JqxGrid, { IGridProps, jqx } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid';
import 'jqwidgets-scripts/jqwidgets/styles/jqx.base.css';
import '../../styles/qenta.css'
import * as utilsText from '../../utils/appText';
import * as utils from '../../utils/utils';
import { Container } from 'react-bootstrap';
import Important from '../customWidgets/important';

class InvoiceGrid extends React.Component<{parentCallback:any}, IGridProps> {
    private myGrid = React.createRef<JqxGrid>();
    private isSelectedRow = false;
    private source: any = {};
    constructor(props: {parentCallback:any}) {
        super(props);
        this.isSelectedRow = false;
        this.myGridOnRowSelect = this.myGridOnRowSelect.bind(this);
        this.myGridOnRowUnselect = this.myGridOnRowUnselect.bind(this);
        this.source = {
            localdata: this.getGridData(),
            datatype: 'json',
            datafields: [
                { name: 'CreationDate', type: 'string' },
                { name: 'Series', type: 'string' },
                { name: 'Customer', type: 'string' },
                { name: 'TotalAmount', type: 'float' },
                { name: 'Status', type: 'string' },
                { name: 'Payment', type: 'string' },
                { name: 'Send', type: 'string' },
                { name: 'Pdf', type: 'bool' },
                { name: 'Xml', type: 'bool' },
                { name: 'Detail', type: 'string' },
                { name: 'Details', type: 'string' }
            ]
        };
        
        const cellsrenderer = (row: number | undefined, columnfield: string | undefined, value: string, defaulthtml: string | undefined, columnproperties: any, rowdata: any): string => {
            if (value == "FDV 100092822") {
                return '<div class="row noOffset" style=" width: 100%; float: ' + columnproperties.cellsalign + '; color: #305E99;">\
                            <div class="col-lg-8 noOffset invoiceLink">\
                                <a href="http://google.com" >' + value + '</a>\
                            </div>' + '<div class="col-lg-4 noOffset">\
                            <span class="tooltip">'+utils.getTooltip(utilsText.tooltipText.attachmentText)+'\
                                <button class="absoluteCenter transparentButton ">'+utils.getSVG(1,utils.getAppColor(2))+'</button>\
                            </span>\
                            <span>\
                                <button class="absoluteCenter transparentButton tooltip">'+utils.getSVG(2,utils.getAppColor(0))+utils.getTooltip(utilsText.tooltipText.creditDevitNotes)+'</button>\
                            </span>\
                        </div>';
            }
            else {
                return '<div class="row noOffset" style=" width: 100%; float: ' + columnproperties.cellsalign + '; color: #305E99;">\
                            <div class="col-lg-8 noOffset invoiceLink">\
                                <a href="http://google.com">' + value + '</a>\
                            </div>\
                            <div class="col-lg-4 noOffset">\
                                <span class="tooltip">'+utils.getTooltip(utilsText.tooltipText.attachmentText)+'\
                                    <button class="absoluteCenter transparentButton">'+utils.getSVG(1,utils.getAppColor(2))+'</button>\
                                </span>\
                                <span>\
                                    <button class="absoluteCenter transparentButton tooltip">'+utils.getSVG(2,utils.getAppColor(0))+utils.getTooltip(utilsText.tooltipText.creditDevitNotes)+'</button>\
                                </span>\
                            </div>';
            }
        };
        const iconSvgStatus = (row: number | undefined, columnfield: string | undefined, value: string, defaulthtml: string | undefined, columnproperties: any, rowdata: any): string => {
            return '<div class="noOffsetDot">\
                        <div class="tooltip" style="width: 10px;">\
                        '+utils.getSVG(3,utils.getAppColor(6))+'\
                        '+utils.getTooltip(utilsText.tooltipText.paymentStatus.duePayment)+'\
                        </div>\
                    </div>';
        };
        const iconSvgProgress = (row: number | undefined, columnfield: string | undefined, value: string, defaulthtml: string | undefined, columnproperties: any, rowdata: any): any => {
            let randomNum: Number = Math.floor((Math.random() * (10-8)) +8);
            return '<div class="iconsStaturBar ">'+utils.getSVG(randomNum,utils.getAppColor(6))+'</div>';
        };
        const iconSvgSend = (row: number | undefined, columnfield: string | undefined, value: string, defaulthtml: string | undefined, columnproperties: any, rowdata: any): string => {
            return '<button class="absoluteCenter transparentButton">' + utils.getSVG(5,utils.getAppColor(2)) + '</button>';
        };
        const iconSvgDownload = (row: number | undefined, columnfield: string | undefined, value: string, defaulthtml: string | undefined, columnproperties: any, rowdata: any): string => {
            return '<button class="absoluteCenter transparentButton">' + utils.getSVG(6,utils.getAppColor(2)) + '</button>';
        };
        const iconSvgXML = (row: number | undefined, columnfield: string | undefined, value: string, defaulthtml: string | undefined, columnproperties: any, rowdata: any): string => {
            return '<button class="absoluteCenter transparentButton">' + utils.getSVG(7,utils.getAppColor(2)) + '</button>';
        };
        const headerRenderer = (defaultText?: string | undefined, alignment?: string | undefined, height?: number | undefined): string => {
            console.log(defaultText);
            if(defaultText == "Consecutivo" ){
                return '<div class="column-text-header-qenta" style="margin-top: 13px;">\
                            <div style="width: 15px;position: absolute;left: 30px;">'+ utils.getSVG(11,utils.getAppColor(2)) +'</div>'+ defaultText + '\
                        </div>';
            }
            return '<div class="column-text-header-qenta">' + defaultText + '</div>'
        };
        const actionButtonsRenderer = (defaultText?: string | undefined, alignment?: string | undefined, height?: number | undefined): string => {
            return '<div class="column-text-header-qenta">\
                        <Row style="display:flex;justify-content: space-around; margin: auto 10px; margin-top: 13px;">\
                            <div>'+utilsText.headerTable.Send+'</div>\
                            <div>'+utilsText.headerTable.Pdf+'</div>\
                            <div>'+utilsText.headerTable.Xml+'</div>\
                            <div>'+utilsText.headerTable.Detail+'</div>\
                        </Row>\
                    </div>';
        };
        const actionButtonsRendererContent = (row: number | undefined, columnfield: string | undefined, value: string, defaulthtml: string | undefined, columnproperties: any, rowdata: any): string => {
            return '<div class="column-text-header-qenta">\
                        <Row style="display:flex;justify-content: space-around; margin: auto 10px;">\
                            <div>\
                                <button class="absoluteCenter transparentButton">' + utils.getSVG(5,utils.getAppColor(2)) + '</button>\
                            </div>\
                            <div class="tooltip">\
                                '+ utils.getTooltip(utilsText.tooltipText.download.invoice) +'\
                                <button class="absoluteCenter transparentButton ">' + utils.getSVG(6,utils.getAppColor(2)) + '</button>\
                            </div>\
                            <div class="">\
                                <button class="absoluteCenter transparentButton tooltip">' + utils.getSVG(7,utils.getAppColor(2)) + utils.getTooltip(utilsText.tooltipText.download.xml) +'</button>\
                            </div>\
                            <div>\
                                <button class="absoluteCenter transparentButton"><a href="/checkInvoice">Ver</a></button>\
                            </div>\
                        </Row>\
                    </div>';
        };
        this.state = {
            columns: [
                { text: utilsText.headerTable.CreationDate, editable: false, datafield: 'CreationDate', width: "110px", align: 'center', cellsalign: "center", renderer: headerRenderer, filterable: false },
                { text: utilsText.headerTable.Series, datafield: 'Series',width: "177px", cellsalign: 'right', align: 'center', cellsrenderer, filterable: false, editable: false, renderer: headerRenderer },
                { text: utilsText.headerTable.Customer, editable: false, datafield: 'Customer',width: "280px", align: 'center', cellsalign: 'left' , filterable: false},
                { text: utilsText.headerTable.TotalAmount, editable: false, datafield: 'TotalAmount',width: "155px", align: 'center', cellsalign: 'right',  cellsformat: 'c2', filterable: false },
                { text: utilsText.headerTable.Status, editable:false, datafield: 'Status',width: "98px",  align: 'center', cellsalign: "center", filterable: false, cellsrenderer: iconSvgProgress, renderer: headerRenderer},
                { text: utilsText.headerTable.Payment, filtertype: 'list',width: "90px", datafield: 'Payment', align: 'center', editable: false, cellsrenderer: iconSvgStatus, cellsalign: "center", sortable: false},
                { text: utilsText.headerTable.Detail, datafield: 'Details', align: 'center', cellsalign: "center", filterable: false, renderer: actionButtonsRenderer, cellsrenderer: actionButtonsRendererContent, sortable: false,editable: false}
            ],
            source: new jqx.dataAdapter(this.source)
        }
        this.props.parentCallback(this.myGrid);
    }
    setSelectedRow(): void{
        if (this.myGrid.current?.getselectedrowindexes().length == 0) {
            this.isSelectedRow = false
        }else{
            this.isSelectedRow = true
        }
        this.props.parentCallback(this.myGrid);
        console.log("setSelectedRow ",this.isSelectedRow)
    }
    getGridData() {
        // this.props.parentCallback({"myGrid": this.myGrid, "data": this.source});
        return [
                {
                    "CreationDate": "06/09/2021",
                    "Series": "FDV 100092822",
                    "Customer": "Daysi Dallana Rincón Díaz",
                    "TotalAmount": 100200000.00,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "<Emitir fill='red'/>",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                },
                {
                    "CreationDate": "06/09/2021",
                    "Series": "FDV 100092823",
                    "Customer": "Julián Alberto Martínez",
                    "TotalAmount": 650750.00,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                },
                {
                    "CreationDate": "07/09/2021",
                    "Series": "FDV 100092824",
                    "Customer": "Centro educativo Los Nogales",
                    "TotalAmount": 1250000,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                },
                {
                    "CreationDate": "08/09/2021",
                    "Series": "FDV 100092825",
                    "Customer": "Luis Rafael Herrera Torrenegra",
                    "TotalAmount": 1250000,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                },
                {
                    "CreationDate": "09/09/2021",
                    "Series": "FDV 100092826",
                    "Customer": "Alejandra Lugo",
                    "TotalAmount": 1250000,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                },
                {
                    "CreationDate": "10/09/2021",
                    "Series": "FDV 100092827",
                    "Customer": "Grupo Industrial del Caribe",
                    "TotalAmount": 1250000,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                },
                {
                    "CreationDate": "11/09/2021",
                    "Series": "FDV 100092828",
                    "Customer": "Esteban Caicedo Moreno",
                    "TotalAmount": 1250000,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                },
                {
                    "CreationDate": "11/09/2021",
                    "Series": "FDV 100092829",
                    "Customer": "Cristaleria Peldar S.A.",
                    "TotalAmount": 1250000,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                },
                {
                    "CreationDate": "12/09/2021",
                    "Series": "FDV 100092830",
                    "Customer": "Daysi Dallana Rincón Díaz",
                    "TotalAmount": 1250000,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                },
                {
                    "CreationDate": "13/09/2021",
                    "Series": "FDV 100092831",
                    "Customer": "Productos Naturales de la Sabana S.A.S.",
                    "TotalAmount": 1250000,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                }

                ,
                {
                    "CreationDate": "13/09/2021",
                    "Series": "FDV 100092831",
                    "Customer": "CLIENTE1",
                    "TotalAmount": 1250000,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                },
                {
                    "CreationDate": "13/09/2021",
                    "Series": "FDV 100092831",
                    "Customer": "CLIENTE1",
                    "TotalAmount": 1250000,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                },
                {
                    "CreationDate": "13/09/2021",
                    "Series": "FDV 100092831",
                    "Customer": "CLIENTE1",
                    "TotalAmount": 1250000,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                },
                {
                    "CreationDate": "13/09/2021",
                    "Series": "FDV 100092831",
                    "Customer": "CLIENTE1",
                    "TotalAmount": 1250000,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                },
                {
                    "CreationDate": "13/09/2021",
                    "Series": "FDV 100092831",
                    "Customer": "CLIENTE1",
                    "TotalAmount": 1250000,
                    "Status": "1",
                    "Payment": "<span class='state'></span><p class='stateText'>Vencida</p>",
                    "Send": "",
                    "Pdf": "",
                    "Xml": "",
                    "Detail": "Ver"
                }
        ]
    }

    public render() {
        if (this.getGridData().length == 0) {
            return (
                <Container className="table_empty">
                    <p className="title_table_empty">Aún no has realizado ninguna factura electrónica. <br></br>
                        <span className="sub_title_table_empty"> Para realizar tu primera factura, haz clic en el (+)</span></p></Container>)
        } else {     
            const position = this.myGrid.current?.scrollposition();
            console.log("Position", position)       
            return (
                <JqxGrid
                    ref={this.myGrid}
                    width="100%" height={"365"}
                    source={this.state.source}
                    columns={this.state.columns}
                    pageable={false}
                    sortable={true}
                    altrows={true}
                    enabletooltips={false}
                    editable={true}
                    columngroups={this.state.columngroups}
                    selectionmode={'checkbox'}
                    filterable = {true}
                    autoshowfiltericon = {false}
                    autoshowcolumnsmenubutton={false}
                    onRowselect={this.myGridOnRowSelect} 
                    onRowunselect={this.myGridOnRowUnselect}

                />
            );
        }
    }
    private myGridOnRowSelect(event: any): void {
        this.setSelectedRow();
        // dispatch({ type: 'UPDATE_INPUT', data: event,});
        // console.log(this.isSelectedRow);
    };
    private myGridOnRowUnselect(event: any): void {
        this.setSelectedRow();
        // console.log(this.isSelectedRow);
    };
}
export default InvoiceGrid;
