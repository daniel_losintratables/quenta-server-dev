import * as React from 'react';
import JqxComboBox, { IComboBoxProps } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxcombobox';
import InputGroup from 'react-bootstrap/InputGroup';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Important from '../customWidgets/important';
import PlusBlack from '../../assets/icons/PlusBlack.svg';
import '../../styles/quentaInput.css'
class Combobox extends React.PureComponent<{placeholder: any, class: string, label: string, icon: string, icon_sup: string, required: any, content: [], height: string, width: string, align: any, comboType: string, open:any}, IComboBoxProps> {
    private myComboBox = React.createRef<JqxComboBox>(); 
    private comboType = "";
    constructor(props: {placeholder: any, class: string, label: string, icon: string, icon_sup: string, required: any, content: [], height: string, width: string, align: any, comboType: string, open:any}) {
        super(props);
        this.state = {
            source: this.generateHTML(),
            comboType: this.props.comboType
        }
        this.onSelect = this.onSelect.bind(this);
    }
    public render() {
        var text_style = "";
        var required = "";
        var icono_sup;
        if (this.props.icon!="") {
            var icono = <img src={this.props.icon} className="icon_search" ></img>
        }
        if (this.props.icon_sup!="") {
            icono_sup = <div className="contain_icon_help"><img src={this.props.icon_sup} className="icon_search" ></img> <Important /></div>
        }
        if (this.props.required==1){
            required = "*";
        }
        return (
            <div className={this.props.class} >
                <Row>
                    <Col lg={12} >
                        <div className="title_search">{this.props.label}{required} {icono_sup}</div> 
                        {/* <InputGroup size="sm" className="inputHeigth" > */}
                            {/* <FormControl aria-label="Text input with checkbox" placeholder={this.props.placeholder} className="input_form" style={{textAlign:this.props.align}} /> */}
                                <JqxComboBox
                                    ref={this.myComboBox} 
                                    width={this.props.width} 
                                    height={this.props.height}
                                    source={this.state.source} 
                                    placeHolder={this.props.placeholder}
                                    searchMode="containsignorecase"
                                    autoDropDownHeight={true}
                                    theme={"combo " + this.props.align}
                                    style={{textAlign:this.props.align}}
                                    onSelect={this.onSelect} 
                                />
                        {/* </InputGroup> */}
                    </Col>
                </Row>
            </div>
        );
    }
    private generateHTML() {
        const source = []
        this.comboType = this.props.comboType;
        if(this.props.comboType == "Cliente"){
            source[0] = ["<div class='add_button_combo'> <div><img src='" + PlusBlack + "'}></img></div>Crear cliente</div>"];
            for (let i = 0; i < this.props.content.length; i++) {
                let title= this.props.content[i];
                const html = "<div>" + title +"</div>";
                source[i+1] = { html, title };
            }
        }else{
            for (let i = 0; i < this.props.content.length; i++) {
                let title= this.props.content[i];
                const html = "<div>" + title +"</div>";
                source[i+1] = { html, title };
            }
        }
        // const source = ["<div>Crear nuevo cliente</div>"];
        
        return source;
    }
    private onSelect(e: Event): void {
        console.log('do something...');
        console.log(e.args.index);
        console.log(this.comboType);
        
        if(this.comboType == "Cliente" && e.args.index == 0){
            this.props.open(true);   
        }
    }
}
export default Combobox;
