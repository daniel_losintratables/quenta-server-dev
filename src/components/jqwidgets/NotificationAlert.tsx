import * as React from 'react';
import JqxNotification from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxnotification';
import Check from '../assets/icons/Check.svg'
import Close from './../../assets/icons/Close.svg';
import './../../styles/notificationAlert.css';

class NotificationAlert extends React.PureComponent<{}> {
    private myNotification = React.createRef<JqxNotification>();
    constructor(props: {}) {
        super(props);
        // this.state = {
        //     openNotification: false
        // }
    }
    componentWillReceiveProps(nextProps: any){
        // console.log("componentWillReceiveProps en NotificationAlert")
        // console.log(nextProps);
        console.log("nextProps.showNotificationAlert", nextProps.showNotificationAlert)
        this.setState({
            openNotification: nextProps.showNotificationAlert
        })
        if(nextProps.showNotificationAlert){
        // if(this.state.openNotification){
            // this.myNotification.current!.setOptions({ template: "" });
            this.myNotification.current!.open();
        }
    }
    public render() {
        return (
            <div >
                <JqxNotification ref={this.myNotification} autoClose={true} autoCloseDelay={100000} 
                    width={'auto'} icon={{ width: 25, height: 25, url:"../../assets/icons/Check.svg", padding: 5 }}
                    template={"null"} opacity={1.0} position={'top-right'} showCloseButton={true}>
                    <div className="okBckNot" >
                        <svg width="100" height="50" viewBox="0 0 92 92" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="46" cy="46" r="44" transform="rotate(-180 46 46)" stroke="white" stroke-width="4"/>
                            <path d="M28 47.1429L40.0897 60L69 30" stroke="white" stroke-width="4" stroke-linecap="round"/>
                        </svg>
                        <p>¡Tu factura xxxxxxxxxx ha sido emitida con éxito! </p>
                        <img className="close_button" src={Close}></img> 
                    </div>
                </JqxNotification>
                {/* <JqxNotification ref={this.msgNotification}
                    width={250} position={'top-right'} opacity={0.9} autoOpen={false}
                    autoClose={true} animationOpenDelay={800} autoCloseDelay={3000} template={'info'}>
                    <div>
                        Welcome to our website.
                    </div>
                </JqxNotification> */}
            </div>
        );
    }
    // private onClose(e: Event): void {
    //     this.props.showNotificationAlert
    // }
    
}
export default NotificationAlert;

