import * as React from 'react';
import JqxScrollView from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxscrollview';
import InvoiceGrid from './InvoiceGrid';

class ScrollView extends React.PureComponent<{parentCallback:any}, {}> {
    private myScrollView = React.createRef<JqxScrollView>();
    private invoiceGrid: any;
    constructor(props: {parentCallback:any}) {
        super(props);
        this.state = {
            scrollViewPage: 0
        }
        // this.onPageChanged = this.onPageChanged.bind(this);
    }
    handleCallback = (childData: any) => {
        console.log("childData");
        console.log(childData);
        // this.setState({ invoiceGrid: childData})
        this.invoiceGrid = childData;
        this.props.parentCallback(this.invoiceGrid);
    }
    componentWillReceiveProps(nextProps: any){
        console.log("componentWillReceiveProps in ScrollView");
        console.log(nextProps);
        
        try {
            this.myScrollView.current!.changePage(nextProps.scrollViewPage);
        } catch (error) {
            console.log("catch");
            console.log(error);
            
        }
        // 
    }
    // onPageChanged() {
    //     console.log("onPageChanged ", this.myScrollView.current?.props.currentPage);
    // }
    render(){
        return(
            <JqxScrollView ref={this.myScrollView} width="95%" height={360} buttonsOffset={[0, 20]}>
                <div>
                    <InvoiceGrid parentCallback = {this.handleCallback}></InvoiceGrid>
                </div>
                <div>
                    <InvoiceGrid parentCallback = {this.handleCallback}></InvoiceGrid>
                </div>
            </JqxScrollView>
        )
    }
}

export default ScrollView