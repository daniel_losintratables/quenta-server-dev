import * as React from 'react';
import JqxGrid, { IGridProps, jqx } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid';
import 'jqwidgets-scripts/jqwidgets/styles/jqx.base.css';
import '../../styles/qenta.css';
import * as utilsText from '../../utils/appText';
import * as globals from '../../utils/globals';
import PlusBlack from '../../assets/icons/PlusBlack.svg';

class CheckInvoiceGrid extends React.Component<{open:any}, IGridProps> {
    private myGrid = React.createRef<JqxGrid>();
    // private isSelectedRow = false;
    private source: any = {};
    constructor(props: {open:any}) {
        super(props);
        // this.isSelectedRow = false;
        let products: any[] = [];
        products.push({value: -1, label: "<div class='add_button_combo'> <div><img src='" + PlusBlack + "'}></img></div>Crear producto</div>"})
        for (let index = 0; index < globals.productList.length; index++) {
            products.push({value: globals.productList[index].Code, label: globals.productList[index].Code +"       "+globals.productList[index].Description});
        }
        const productsSource: any = {
            datafields: [
                { name: 'label', type: 'string' },
                { name: 'value', type: 'string' }
            ],
            datatype: 'array',
            localdata: products
        };
        const productsAdapter: any = new jqx.dataAdapter(productsSource);
        const handlekeyboardnavigation: IGridProps['handlekeyboardnavigation'] = (event) => {
            const key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
            if (key === 9) {
                let totalRows: string | undefined;
                // alert('Pressed Tab.');
                if(this.myGrid.current!.getdatainformation().rowscount != undefined && this.myGrid.current!.getselectedcells()[0] != undefined){
                    totalRows = this.myGrid.current!.getdatainformation().rowscount;
                    let currentColumn = this.myGrid.current!.getselectedcells()[0].datafield;
                    totalRows -= 1;
                    if(this.myGrid.current!.getselectedcells()[0].rowindex == totalRows && currentColumn == "Total" ){
                        this.myGrid.current!.addrow(1,{},"last");
                    }
                }else{
                    console.log("es undefined");
                }
                // return true;
            }
            else if (key === 27) {
                // alert('Pressed Esc Key.');
                return true;
            }
            else if (key === 40) {
                // alert('Pressed Down.');
                console.log(this.myGrid.current!.getdatainformation().rowscount);  
                this.myGrid.current!.addrow(1,{},"last");
                return true;
            }
            else if (key === 117) {
                alert('Pressed F6');
                return true;
            }
            else if (key === 115) {
                // alert('Pressed F4.');
                console.log(this.myGrid.current!.getdatainformation().rowscount);
                let rowData = this.myGrid.current!.getrowdata(this.myGrid.current!.getdatainformation().rowscount - 1);
                console.log(rowData.Code);
                console.log(rowData);
                this.myGrid.current!.addrow(1,{"Code": rowData.Code,"Description": rowData.Description,"Qty": rowData.Qty,"UnitValue": rowData.UnitValue,"Total": rowData.Total}, "last");
                return true;
            }
            else if (key === 116) {
                console.log('Pressed F5.');
                console.log(this.myGrid.current!.getdatainformation().rowscount);
                if (this.myGrid.current!.getselectedcells().length == 1 && this.myGrid.current!.getdatainformation().rowscount > 1){
                    this.myGrid.current!.deleterow(""+this.myGrid.current!.getselectedcells()[0].rowindex);
                }else{

                }
                return true;
            }
            return false;
        };
        this.source = {
            localdata: this.getGridData(),
            datatype: 'json',
            datafields: [
                { name: 'Code', value: 'Code', values: { source: productsAdapter.records, value: 'value', name: 'label' } },
                { name: 'Description', type: 'string' },
                { name: 'Qty', type: 'string' },
                { name: 'UnitValue', type: 'float' },
                { name: 'Discount', type: 'string' },
                { name: 'Total', type: 'string' }
            ]
        };
        this.state = {
            columns: [
                {
                    cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
                        if (newvalue !== oldvalue) {
                            for (let i = 0; i < globals.productList.length; i++) {
                                if(globals.productList[i].Code == newvalue){
                                    this.myGrid.current!.setcellvalue(row, 'Description', globals.productList[i].Description);
                                    this.myGrid.current!.setcellvalue(row, 'UnitValue', globals.productList[i].UnitValue);
                                }
                            }
                        };
                    },
                    columntype: 'combobox', 
                    createeditor: (row: number, value: any, editor: any): void => {
                        editor.jqxComboBox({ width: "25%",dropDownWidth:320, source: productsAdapter, displayMember: 'label', valueMember: 'value', searchMode: 'containsignorecase' });
                    },
                    datafield: 'Code', 
                    displayfield: 'Code', 
                    text: 'Código',
                    width: "139px",
                    filterable: false,
                    align: 'center'            
                },
                { 
                    text: utilsText.headerInvoiceTable.Description, 
                    editable: true, 
                    datafield: 'Description',
                    width: "532px", 
                    cellsalign: 'left', 
                    align: 'center', 
                    filterable: false
                },
                { 
                    text: utilsText.headerInvoiceTable.Qty, 
                    editable: true, 
                    datafield: 'Qty',
                    width: "113px", 
                    align: 'center', 
                    cellsalign: 'right', 
                    filterable: false,
                    cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
                        if (newvalue !== oldvalue) {
                            console.log(newvalue)
                            let totalAmount = this.calcTotal(newvalue,this.myGrid.current!.getcellvalue(row, 'UnitValue'),this.myGrid.current!.getcellvalue(row, 'Discount'));
                            this.myGrid.current!.setcellvalue(row,'Total',totalAmount);
                        };
                    },
                },
                { 
                    text: utilsText.headerInvoiceTable.UnitValue, 
                    editable: true, 
                    datafield: 'UnitValue',
                    width: "148px", 
                    align: 'center', 
                    cellsalign: 'right', 
                    filterable: false, 
                    cellsformat: 'c2',
                    cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
                        if (newvalue !== oldvalue) {
                            console.log(newvalue)
                            let totalAmount = this.calcTotal(this.myGrid.current!.getcellvalue(row, 'Qty'),newvalue,this.myGrid.current!.getcellvalue(row, 'Discount'));
                            this.myGrid.current!.setcellvalue(row,'Total',totalAmount);
                        };
                    },
                },
                { 
                    text: utilsText.headerInvoiceTable.Discount, 
                    editable:true, 
                    datafield: 'Discount',
                    width: "96px",  
                    align: 'center', 
                    cellsalign: "right", 
                    filterable: false, 
                    cellsformat: 'c2',
                    cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
                        if (newvalue !== oldvalue) {
                            console.log(newvalue)
                            let totalAmount = this.calcTotal(this.myGrid.current!.getcellvalue(row, 'Qty'),this.myGrid.current!.getcellvalue(row, 'UnitValue'),newvalue);
                            this.myGrid.current!.setcellvalue(row,'Total',totalAmount);
                        };
                    },
                },
                { 
                    text: utilsText.headerInvoiceTable.Total, 
                    editable: false, 
                    width: "170px", 
                    datafield: 'Total', 
                    align: 'center',  
                    cellsalign: "right", 
                    filterable: false, 
                    cellsformat: 'c2'
                },
            ],
            handlekeyboardnavigation,
            source: new jqx.dataAdapter(this.source)
        }
        console.log("Renderizo NewinvoiceGrid");
    }

    getGridData() {
        // this.props.parentCallback({"myGrid": this.myGrid, "data": this.source});
        return [
            {
                Code: "1234567899123456",
                Description: "Combo Lata de pintura x Galón - Blanco Esmaltado",
                Qty: "23,00",
                UnitValue: "$2.000.000.000,00",
                Discount: "19%",
                Total: "$2.000.000.000,00"
            },
            {
                Code: "9999999999999999",
                Description: "Combo Lata de pintura x Galón - Blanco Esmaltado con brocha Combo Lata de pintura x Galón Combo Lata de pintura x Galón - Blanco Esmaltado con brocha Combo Lata de pintura x Galón - Blanco Esmaltado con brocha  Galón - Blanco Esmaltado con brocha Combo Lata de pintura x Galón - Blanco Esmaltado con brocha",
                Qty: "23,00",
                UnitValue: "$2.000.000.000,00",
                Discount: "19%",
                Total: "$2.000.000.000,00"
            },
            {
                Code: "123334",
                Description: "Combo Lata de pintura x Galón - Blanco Esmaltado",
                Qty: "23,00",
                UnitValue: "$2.000.000.000,00",
                Discount: "19%",
                Total: "$2.000.000.000,00"
            },
            {
                Code: "9999999999999999",
                Description: "Combo Lata de pintura x Galón - Blanco Esmaltado",
                Qty: "23,00",
                UnitValue: "$2.000.000.000,00",
                Discount: "19%",
                Total: "$2.000.000.000,00"
            },
            {
                Code: "7777777777777777",
                Description: "Combo Lata de pintura x Galón - Blanco Esmaltado",
                Qty: "23,00",
                UnitValue: "$2.000.000.000,00",
                Discount: "19%",
                Total: "$2.000.000.000,00"
            },
            {
                Code: "99999934563278",
                Description: "Combo Lata de pintura x Galón - Blanco Esmaltado con brocha Combo Lata de pintura x Galón Combo Lata de pintura x Galón - Blanco Esmaltado con brocha",
                Qty: "23,00",
                UnitValue: "$2.000.000.000,00",
                Discount: "19%",
                Total: "$2.000.000.000,00"
            },
            {
                Code: "1234567899123456",
                Description: "Combo Lata de pintura x Galón - Blanco Esmaltado",
                Qty: "23,00",
                UnitValue: "$2.000.000.000,00",
                Discount: "19%",
                Total: "$2.000.000.000,00"
            },
            {
                Code: "8888888888888888",
                Description: "Combo Lata de pintura x Galón - Blanco Esmaltado",
                Qty: "23,00",
                UnitValue: "$2.000.000.000,00",
                Discount: "19%",
                Total: "$2.000.000.000,00"
            },
            {
                Code: "1234",
                Description: "Combo Lata de pintura x Galón - Blanco Esmaltado",
                Qty: "23,00",
                UnitValue: "$2.000.000.000,00",
                Discount: "19%",
                Total: "$2.000.000.000,00"
            },
            {
                // Code: "",
                // Description: "Combo Lata de pintura x Galón - Blanco Esmaltado",
                // Qty: "23,00",
                // UnitValue: "$2.000.000.000,00",
                // Discount: "19%",
                // Total: "$2.000.000.000,00"
            }
        ]
    }
    calcTotal(qty: any, unitPrice: any, discount: any){
        console.log("calcTotal");
        console.log(qty, unitPrice,discount)
        console.log((qty * unitPrice) - (discount?discount:0));
        return (qty * unitPrice) - (discount?discount:0);
    }
    public render() { 
        return (
            <JqxGrid
                ref={this.myGrid}
                width="100%"
                autoheight={true}
                source={this.state.source}
                columns={this.state.columns}
                pageable={false}
                sortable={true}
                altrows={true}
                enabletooltips={false}
                editable={true}
                columngroups={this.state.columngroups}
                selectionmode={'multiplecellsadvanced'}
                filterable = {true}
                autoshowfiltericon = {true}
                autoshowcolumnsmenubutton={true}
                autorowheight={true}
                handlekeyboardnavigation={this.state.handlekeyboardnavigation}
            />
            
        );
    }
}
export default CheckInvoiceGrid;
