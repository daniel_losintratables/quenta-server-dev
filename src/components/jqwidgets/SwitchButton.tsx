import * as React from 'react';
import JqxSwitchButton, { ISwitchButtonProps } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxswitchbutton';
import Save from '../../assets/icons/Save.svg';
import '../../styles/switchButton.css';
import Tooltip from '../customWidgets/tooltip'

class SwitchButton extends React.PureComponent<{}, ISwitchButtonProps> {
    private events = React.createRef<HTMLDivElement>();
    constructor(props: {}) {
        super(props);
        this.checkedEvent = this.checkedEvent.bind(this);
        this.uncheckedEvent = this.uncheckedEvent.bind(this);
        
    }
    public render() {
        
        return (
            <div className="switch-button">
                <JqxSwitchButton onChecked={this.checkedEvent} onUnchecked={this.uncheckedEvent}
                    width={81} height={27} checked={true} />
                <div ref={this.events} hidden className="tooltip"><img src={Save}></img><Tooltip rotate={0} content={"Guardar manualmente"}/></div>
            </div>
        );
    }
    // Event handling
    private checkedEvent(event: any): void {
        const parent = event.target.parentNode;
        const key = parent.dataset.key;
        this.events.current!.hidden = true;
    }
    private uncheckedEvent(event: any): void {
        const parent = event.target.parentNode;
        const key = parent.dataset.key;
        this.events.current!.hidden = false;
    }
}
export default SwitchButton;