import * as React from 'react';
import JqxDateTimeInput from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxdatetimeinput';
import InputGroup from 'react-bootstrap/InputGroup';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Important from '../customWidgets/important';
import '../../styles/quentaInput.css';

class DateInput extends React.PureComponent<{class: string, placeholder: string, icon: string, label: string, icon_sup: string, required: any, align: string}> {
    constructor(props: {class: string, placeholder: string, icon: string, label: string, icon_sup: string, required: any, align: string}) {
        super(props);
        this.state = {
            class: props.class,
            placeholder: props.placeholder,
            icon: props.icon,
            label: props.label,
            icon_sup: props.icon_sup,
            required: props.required,
            align: props.align,
        }
    }
    public render() {
        var text_style = "";
        var icono_sup = <div></div>;
        var required = "";
        if (this.props.icon!="") {
            var icono = <img src={this.props.icon} className="icon_search" ></img>
        }
        if (this.props.icon_sup!="") {
            icono_sup = <div className="contain_icon_help"><img src={this.props.icon_sup} className="icon_search" ></img> <Important /></div>
        }
        if (this.props.required==1){
            required = "*";
        }
        return (
            // <div>
                <div className={this.props.class}>
                    <Row>
                        <Col lg={12} >
                            <div className="title_search">{this.props.label}{required} {icono_sup}</div> 
                            <InputGroup size="sm" className="inputHeigth">
                                <JqxDateTimeInput width={320} height={25}
                                culture="es-ES"
                                formatString= {'dd MMMM yyyy'}
                                textAlign={'center'}
                                />
                            </InputGroup>
                        </Col>
                    </Row>
                </div>
            // </div>
        );
    }
}
export default DateInput;