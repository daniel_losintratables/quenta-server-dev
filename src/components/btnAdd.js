import React, { Component } from 'react';

import EmitirGris from '../assets/icons/EmitirGris.svg';
import Issue from '../assets/icons/Issue.svg';
import File from '../assets/icons/File.svg';
import Add from '../assets/icons/Add.svg';
import ClosedMenu from '../assets/icons/ClosedMenu.svg';
import Factura from  '../assets/icons/Factura.svg';
import Debito from  '../assets/icons/Debito.svg';
import Credito from  '../assets/icons/Credito.svg';
import Download from '../assets/icons/Download.svg'
import XML from '../assets/icons/Xml.svg';
import Delete from '../assets/icons/Delete.svg'
import Resend from '../assets/icons/Resend.svg'
import Success from '../assets/icons/Success.svg'
import * as utils from '../utils/utils';
import * as utilsText from '../utils/appText';
import './../styles/bottomModal.css';
import Popup from 'reactjs-popup';
import NewClient from './customWidgets/newClient';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import NotificationAlert from './jqwidgets/NotificationAlert';
import InvoiceAlert from './customWidgets/invoiceAlert';
import NewProduct from './customWidgets/newProducto';
import { BrowserRouter, Route, NavLink } from "react-router-dom";



class BtnAdd extends Component{
    
    constructor(props){
        super(props);
        let buttonClass = "modalGreenButton";
        let buttonIcon = Add;
        this.state = {
            showModal: false,
            showNotificationAlert: false,
            invoiceGrid: 0,
            fromPage: props.fromPage
        }
        this.clickShowNotificationAlert = this.clickShowNotificationAlert.bind(this);
        
        this.button_modal = React.createRef();
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside);
    }
    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside);
    }

    handleClickOutside(event) {
        if (!this.button_modal.current.contains(event.target)) {
            if (this.state.showModal){
                this.clickShowModal()
            }
        }
    }


    clickShowModal(){
        this.setState({
            showModal: !this.state.showModal
        });
    }
    clickShowNotificationAlert(){
        console.log("clickShowNotificationAlert");
        this.setState({
            showNotificationAlert: !this.state.showNotificationAlert
        });
        console.log("this.state", this.state.showNotificationAlert);
    }
    componentWillReceiveProps(nextProps){
        if (this.state.fromPage == "mainPage") {
            this.setState({
                invoiceGrid: nextProps.invoiceGrid
            });
            try {
                if(nextProps.invoiceGrid.current?.getselectedrowindexes().length != 0 && nextProps.invoiceGrid.current?.getselectedrowindexes().length != undefined){
                    this.buttonClass = "modalOrangeButton";
                    this.buttonIcon = ClosedMenu;
                }else{
                    this.buttonClass = "modalGreenButton";
                    this.buttonIcon = Add;
                }
            } catch (error) {
                this.buttonClass = "modalGreenButton";
                this.buttonIcon = Add;
            }
        }else if(this.state.fromPage == "newInvoice"){

        }
    } 
    showToastMessage(content){
        console.log("ddddd");
        // toast.success(content, {
        //     position: "bottom-right",
        //     autoClose: 5000,
        //     hideProgressBar: true,
        //     closeOnClick: true,
        //     pauseOnHover: true,
        //     draggable: true,
        //     progress: undefined,
        //     toastId: 1
        // });
        toast(content, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            });
    }
    render(){
        if (this.state.fromPage == "mainPage") {
            return(
                <div className='modalDialogue' ref={this.button_modal}>
                    {
                        this.state.showModal ?
                        <div className={this.buttonClass == "modalOrangeButton"?"dialogueOrange":"dialogue"}>
                            <p className="dialogueTitle" >
                            ¿Qué quieres hacer?
                            </p>
                            {
                                this.state.invoiceGrid.current?.getselectedrowindexes().length != 0 && this.state.invoiceGrid.current?.getselectedrowindexes().length != undefined?
                                <p className="optionButtonDialogueOrange" >
                                Tienes ({this.state.invoiceGrid.current?.getselectedrowindexes().length}) elementos {this.state.invoiceGrid.current?.getselectedrowindexes().length == 1?"seleccionado":"seleccionados"}
                                </p>:""
                            }
                            {
                                this.state.invoiceGrid.current?.getselectedrowindexes().length !=0 && this.state.invoiceGrid.current?.getselectedrowindexes().length != undefined?
                                <div>
                                    <p className="optionButtonDialogueOrange">
                                    <span>
                                    <img className="optionIconMargin" src={XML} >
                                        </img> 
                                    </span>
                                    Descargar selección (XML)
                                    </p>
                                    <p className="optionButtonDialogueOrange">
                                    <span>
                                    <img className="optionIconMargin" src={Download} >
                                        </img> 
                                    </span>
                                    Descargar selección (PDF)
                                    </p>
                                    <p className="optionButtonDialogueOrange">
                                    <span>
                                    <img className="optionIconMargin" src={Delete} >
                                        </img> 
                                    </span>
                                    Elminar factura
                                    </p>
                                </div>
                                :
                                <div>
                                
                                    {/* <Popup
                                        trigger={<button className="transparentButton"> 
                                            <p className="optionButtonDialogue">
                                                <span>
                                                <img className="optionIconMargin" src={Factura} >
                                                    </img> 
                                                </span>
                                                Crear factura 
                                            </p>
                                        </button>}
                                        modal
                                        nested
                                    >
                                    {close => (
                                        <NewClient></NewClient>
                                    )}
                                    </Popup> */}
                                    <NavLink to="/newInvoice">
                                        <button className="transparentButton"> 
                                            <p className="optionButtonDialogue">
                                                <span>
                                                <img className="optionIconMargin" src={Factura} >
                                                    </img> 
                                                </span> 
                                                Crear factura 
                                            </p>
                                        </button>
                                    </NavLink>
                                    <Popup
                                        trigger={<button className="transparentButton">
                                            <p className="optionButtonDialogue">
                                                        <span>
                                                        <img className="optionIconMargin" src={Debito} >
                                                            </img> 
                                                        </span>
                                                        Crear nota débito
                                                        </p>
                                            </button>}
                                        modal
                                        nested
                                    >
                                    {close => (
                                        <NewProduct></NewProduct>
                                        // <NewClient></NewClient>
                                    )}
                                    </Popup>
                                    
                                    
                                    <p className="optionButtonDialogue">
                                    <span>
                                    <img className="optionIconMargin" src={Credito} >
                                        </img> 
                                    </span>
                                    Crear nota crédito
                                    </p>
                                </div>
                            }
                            {/* <p className="optionButtonDialogue">
                            <span>
                            <img className="optionIconMargin" src={Factura} >
                                </img> 
                            </span>
                            Crear factura 
                            </p>
                            <p className="optionButtonDialogue">
                            <span>
                            <img className="optionIconMargin" src={Debito} >
                                </img> 
                            </span>
                            Crear nota débito
                            </p>
                            <p className="optionButtonDialogue">
                            <span>
                            <img className="optionIconMargin" src={Credito} >
                                </img> 
                            </span>
                            Crear nota crédito
                            </p> */}
    
                        </div>
                        : null
                    }
                <button onClick={()=>this.clickShowModal()}  className="modalButton" >
                <div className={this.buttonClass != undefined?this.buttonClass:"modalGreenButton"}>
                    <div className="modalIcon"  style={this.state.showModal == true?{transform: "rotate(45deg)"}:{}}>
                            {
                                this.state.showModal?
                                <img src={Add} >
                                </img>
                                :
                                <img src={this.buttonIcon?this.buttonIcon:Add} >
                                </img>
                            }
                        </div>
                </div> 
                </button>
                </div>
            )
        }else if(this.state.fromPage == "newInvoice"){
            return(
                <div className='modalDialogue' ref={this.button_modal}>
                    <NotificationAlert showNotificationAlert = {this.state.showNotificationAlert}/>
                    {
                        this.state.showModal ?
                        <div className="dialogueOrange">
                            <p className="dialogueTitle dialogue_new_invoice" >
                            ¿Qué quieres hacer con tu factura?
                            </p>
                            {
                                // this.state.invoiceGrid.current?.getselectedrowindexes().length != 0 && this.state.invoiceGrid.current?.getselectedrowindexes().length != undefined?
                                // <p className="optionButtonDialogueOrange" >
                                // Tienes ({this.state.invoiceGrid.current?.getselectedrowindexes().length}) elementos {this.state.invoiceGrid.current?.getselectedrowindexes().length == 1?"seleccionado":"seleccionados"}
                                // </p>:""
                            }
                            {
                                <div>
                                    <button onClick={this.clickShowNotificationAlert} className="transparentButton"> 
                                        <p className="optionButtonDialogue">
                                            <span>
                                            <img className="optionIconMargin" src={Issue} >
                                                </img> 
                                            </span>
                                            {utilsText.modalNewInvoice.sendInvoice }
                                        </p>
                                    </button>
                                    {/* <button onClick={() => this.showToastMessage("Tu factura xxxxxxxxxxx ha sido emitida con éxito!")} className="transparentButton"> 
                                        <p className="optionButtonDialogue">
                                            <span>
                                            <img className="optionIconMargin" src={Emitir} >
                                                </img> 
                                            </span>
                                            {utilsText.modalNewInvoice.sendInvoice }
                                        </p>
                                    </button> */}
                                    {/* <ToastContainer
                                        className="toast_container"
                                        position="top-right"
                                        autoClose={5000}
                                        hideProgressBar
                                        newestOnTop
                                        closeOnClick
                                        rtl={false}
                                        pauseOnFocusLoss
                                        draggable
                                        pauseOnHover
                                    /> */}
                                    
                                    <p className="optionButtonDialogue">
                                    <span>
                                    <img className="optionIconMargin" src={Download} >
                                        </img> 
                                    </span>
                                    {utilsText.modalNewInvoice.downloadInovice}
                                    </p>
                                    <p className="optionButtonDialogue">
                                    <span>
                                    <img className="optionIconMargin" src={Delete} >
                                        </img> 
                                    </span>
                                    {utilsText.modalNewInvoice.deleteInvoice}
                                    </p>
                                </div>
                            }
                        </div>
                        : null
                    }
                <button onClick={()=>this.clickShowModal()}  className="modalButton" >
                    <div className="modalOrangeButton">
                        <div className="modalIcon"  style={this.state.showModal == true?{transform: "rotate(45deg)"}:{}}>
                            {
                                this.state.showModal?
                                <img src={Add} >
                                </img>
                                :
                                <img src={this.buttonIcon?this.buttonIcon:ClosedMenu} >
                                </img>
                            }
                        </div>
                    </div> 
                </button>
                </div>
            )
        } else if(this.state.fromPage == "checkInvoice"){
            return(
                <div className='modalDialogue checkInvoice' ref={this.button_modal}  >
                    <NotificationAlert showNotificationAlert = {this.state.showNotificationAlert}/>
                    {
                        this.state.showModal ?
                        <div className="dialogueOrange">
                            <p className="dialogueTitle dialogue_new_invoice" >
                            ¿Qué quieres hacer con tu factura?
                            </p>
                            {
                                <div>
                                    <p className="optionButtonDialogue">
                                        <span>
                                        <img className="optionIconMargin" src={Resend} >
                                            </img> 
                                        </span>
                                        {utilsText.modalCheckInvoice.sendInvoice }
                                    </p>
                                    <p className="optionButtonDialogue">
                                        <span>
                                        <img className="optionIconMargin" src={File} >
                                            </img> 
                                        </span>
                                        {utilsText.modalCheckInvoice.downloadXml}
                                    </p>
                                    <p className="optionButtonDialogue">
                                        <span>
                                        <img className="optionIconMargin" src={Download} >
                                            </img> 
                                        </span>
                                        {utilsText.modalCheckInvoice.downloadInovice}
                                    </p>
                                    <p className="optionButtonDialogue">
                                        <span>
                                        <img className="optionIconMargin" src={Delete} >
                                            </img> 
                                        </span>
                                        {utilsText.modalCheckInvoice.deleteInvoice}
                                    </p>
                                </div>
                            }
                        </div>
                        : null
                    }
                <button onClick={()=>this.clickShowModal()}  className="modalButton" >
                    <div className="modalOrangeButton">
                        <div className="modalIcon"  style={this.state.showModal == true?{transform: "rotate(45deg)"}:{}}>
                            {
                                this.state.showModal?
                                <img src={Add} >
                                </img>
                                :
                                <img src={this.buttonIcon?this.buttonIcon:ClosedMenu} >
                                </img>
                            }
                        </div>
                    </div> 
                </button>
                </div>
            )
        }
        
    }
}

export default BtnAdd;
